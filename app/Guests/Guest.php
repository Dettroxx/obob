<?php

namespace App\Guests;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guest extends Model 
{

    protected $table = 'guests';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('ip', 'name', 'status', 'earning_percent', 'remuneration_percent', 'is_shop', 'shop_percent', 'alerts', 'income');
    protected $with = ['referer'];

    public function referer() {
        return $this->hasOne('App\Guests\GuestReferalLink', 'to', 'id'); 
    }
}