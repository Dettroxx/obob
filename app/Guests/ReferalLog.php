<?php

namespace App\Guests;

use Illuminate\Database\Eloquent\Model;

class ReferalLog extends Model 
{

    protected $table = 'referal_logs';
    public $timestamps = true;
    protected $fillable = array('bid_id', 'payment_id', 'guest_id');

}