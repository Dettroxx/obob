<?php

namespace App\Guests;

use Illuminate\Database\Eloquent\Model;

class GuestStatus extends Model 
{

    protected $table = 'guest_statuses';
    public $timestamps = false;
    protected $fillable = array('name');

}