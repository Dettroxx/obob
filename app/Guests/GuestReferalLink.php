<?php

namespace App\Guests;

use Illuminate\Database\Eloquent\Model;

class GuestReferalLink extends Model 
{

    protected $table = 'guests_referal_links';
    public $timestamps = false;
    protected $fillable = array('from', 'to');

    //Связь с guest
    public function to() {
        return $this->belongsTo('App\Guests\Guest', 'to', 'id');
    }

    public function from() {
        return $this->belongsTo('App\Guests\Guest', 'from', 'id');
    }
}