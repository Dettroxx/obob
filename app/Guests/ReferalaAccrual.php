<?php

namespace App\Guests;

use Illuminate\Database\Eloquent\Model;

class ReferalaAccrual extends Model 
{

    protected $table = 'refferal_accruals';
    public $timestamps = true;
    protected $fillable = array('guest_id', 'total', 'account', 'payment_system_id', 'type');

}