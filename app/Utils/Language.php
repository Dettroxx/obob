<?php

namespace App\Utils;

use Illuminate\Database\Eloquent\Model;

class Language extends Model 
{

    protected $table = 'languages';
    public $timestamps = true;
    protected $fillable = array('key', 'name', 'icon');

}