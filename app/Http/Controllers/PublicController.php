<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pages\Page;
use App\Pages\Faq\FaqCategory;
use App\Pages\Faq\Faq;
use App\Pages\News\OneNew;

class PublicController extends Controller
{
    //Обмен валюты
    public function index() {
        return view('pages/exchange');
    }


    //Правила обмена
    public function rules() {
        $rules = Page::where("family", "=", "rules")->first();
        return view('pages/page', [
            'page' => $rules,
            'breadcrumbs' => [ 
                '/' => 'Главная',
                '/partners' => $rules->name,
            ],
        ]);
    }


    //Партнерам
    public function partners() {
        $partners = Page::where("family", "=", "partners")->first();
        return view('pages/page', [
            'page' => $partners,
            'breadcrumbs' => [ 
                '/' => 'Главная',
                '/partners' => $partners->name,
            ],
        ]);
    }


    //Новости
    public function news() {
        $news = OneNew::orderBy("updated_at", "desc")->orderBy("created_at", "desc")->paginate(15);
        return view('pages/news', [
            'page' => $news,
            'title' => 'Новости',
            'breadcrumbs' => [
                '/' => 'Главная',
                '/news' => 'Новости'
            ]
        ]);
    }

    
    //Одна новость
    public function new_one($id) {
        $new = OneNew::where("id", "=", $id)->first();
        return view('pages/page', [
            'page' => $new,
            'title' => $new->name,
            'breadcrumbs' => [
                '/' => 'Главная',
                '/news' => 'Новости',
                '/news/'.$new->id => $new->name
            ]
        ]);
    }


    //Faq
    public function faq() {
        $categories = (array) FaqCategory::with("faqs")->get()->toArray();
        return view('pages/faq', [
            'categories' => $categories,
            'breadcrumbs' => [ 
                '/' => 'Главная',
                '/faq' => 'Faq',
            ],
        ]);
    }


    //Faq-one
    public function faq_one($id) {
        $faq = Faq::where("id", "=", $id)->first();
        return view('pages/page', [
            'page' => $faq->toArray(),
            'breadcrumbs' => [ 
                '/' => 'Главная',
                '/faq' => 'Faq',
                '/faq/'.$id => $faq->name,
            ],
        ]);
    }


    //Контакты
    public function contacts() {
        $contacts = Page::where("family", "=", "contacts")->first();
        return view('pages/page', [
            'page' => $contacts,
            'breadcrumbs' => [ 
                '/' => 'Главная',
                '/contacts' => $contacts->name,
            ],
        ]);
    }
}
