<?php

namespace App\PaymentSystem;

use Illuminate\Database\Eloquent\Model;

class PaymentField extends Model 
{

    protected $table = 'payment_fields';
    public $timestamps = false;
    protected $fillable = array('payment_system_id', 'key', 'info', 'icon', 'required', 'mask', 'show_out', 'show_in', 'show_wallet');

}