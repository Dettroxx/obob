<?php

namespace App\PaymentSystem;

use Illuminate\Database\Eloquent\Model;

class PaymentFieldValue extends Model 
{

    protected $table = 'payment_field_values';
    public $timestamps = false;
    protected $fillable = array('payment_field', 'value', 'out_in');

}