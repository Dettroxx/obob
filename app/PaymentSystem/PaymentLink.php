<?php

namespace App\PaymentSystem;

use Illuminate\Database\Eloquent\Model;

class PaymentLink extends Model 
{

    protected $table = 'payment_links';
    public $timestamps = false;
    protected $fillable = array('extra', 'from', 'to', 'active', 'reverse', 'course', 'corridor_from', 'corridor_to', 'corridor_active', 'alert');

    public function from_payment() {
        return $this->hasOne('App\PaymentSystem\PaymentSystem', 'id', 'from'); 
    }

    public function to_payment() {
        return $this->hasOne('App\PaymentSystem\PaymentSystem', 'id', 'to'); 
    }
}