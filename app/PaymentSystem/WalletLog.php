<?php

namespace App\PaymentSystem;

use Illuminate\Database\Eloquent\Model;

class WalletLog extends Model 
{

    protected $table = 'wallet_log';
    public $timestamps = true;
    protected $fillable = array('wallet', 'in_out', 'bid_id', 'cost', 'online');

}