<?php

namespace App\PaymentSystem;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentSystem extends Model 
{

    protected $table = 'payment_systems';
    public $timestamps = true;
    protected $with = ['wallets_stat']; 

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('icon', 'name', 'in', 'out', 'referal_out', 'precise', 'abbr', 'active', 'type');

    public function wallets() {
        return $this->hasMany('App\PaymentSystem\Wallet', 'payment_system_id', 'id'); 
    }

    public function wallets_stat() {
        return $this->hasMany('App\PaymentSystem\Wallet', 'payment_system_id', 'id');
    }

    protected $appends = ['WalletsCount', 'WalletMax'];
    public function getWalletsCountAttribute(){
        return $this->hasMany('App\PaymentSystem\Wallet')->where('payment_system_id', $this->id)->count();
    }
    public function getWalletMaxAttribute(){
        return $this->hasMany('App\PaymentSystem\Wallet')->where('payment_system_id', $this->id)->sum('reserve');
    }
}