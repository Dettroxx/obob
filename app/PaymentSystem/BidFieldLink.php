<?php

namespace App\PaymentSystem;

use Illuminate\Database\Eloquent\Model;

class BidFieldLink extends Model 
{

    protected $table = 'bid_fields_links';
    public $timestamps = false;
    protected $fillable = array('bid_id', 'value_id');

}