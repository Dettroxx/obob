<?php

namespace App\PaymentSystem;

use Illuminate\Database\Eloquent\Model;

class WalletFieldLink extends Model 
{

    protected $table = 'wallet_fields_link';
    public $timestamps = false;
    protected $fillable = array('wallet_id', 'value_id');

}