<?php

namespace App\PaymentSystem;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wallet extends Model 
{

    protected $table = 'wallets';
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = array('payment_system_id', 'code', 'name', 'reserve', 'active', 'max_limit');

    protected $casts = [
        'fields' => 'json',
    ];

    public function payment_system() {
        return $this->belongsTo('App\PaymentSystem\PaymentSystem', 'id', 'payment_system_id');
    }
}