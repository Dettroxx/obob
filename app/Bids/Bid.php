<?php

namespace App\Bids;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\PaymentSystem\PaymentLink;

class Bid extends Model 
{

    protected $table = 'bids';
    public $timestamps = true;

    protected $fillable = [
        'from',
        'to',
        'operator',
        'guest_id',
        'status',
        'value_from',
        'value_to',
        'trash'
    ];

    protected $appends = ['PaymentLink'];
    public function getPaymentLinkAttribute(){
        return PaymentLink::
        where('from', "=", $this->from)
        ->where('to', "=", $this->to)
        ->first();
    }

    protected $with = ['guest_data', 'status_data', 'payment_from_data','payment_to_data','from_fields','to_fields'];

    public function guest_data() {
        return $this->hasOne('App\Guests\Guest', 'id', 'guest_id'); 
    }

    public function status_data() {
        return $this->hasOne('App\Bids\BidStatus', 'id', 'status'); 
    }

    public function payment_from_data() {
        return $this->hasOne('App\PaymentSystem\PaymentSystem', 'id', 'from'); 
    }

    public function payment_to_data() {
        return $this->hasOne('App\PaymentSystem\PaymentSystem', 'id', 'to'); 
    }

    public function from_fields() {
        return $this->hasMany('App\Bids\BidFieldValue', 'bid', 'id')->where('out_in', "=", false);
    }

    public function to_fields() {
        return $this->hasMany('App\Bids\BidFieldValue', 'bid', 'id')->where('out_in', "=", true);
    }
}