<?php

namespace App\Bids;

use Illuminate\Database\Eloquent\Model;

class BidStatus extends Model 
{

    protected $table = 'bid_statuses';
    public $timestamps = false;
    protected $fillable = array('name');

}