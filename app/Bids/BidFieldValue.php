<?php

namespace App\Bids;

use Illuminate\Database\Eloquent\Model;

class BidFieldValue extends Model 
{

    protected $table = 'bid_field_values';
    public $timestamps = false;
    protected $fillable = array('bid', 'field_id', 'value', 'out_in');
    protected $with = ['field'];

    public function field() {
        return $this->hasOne('App\PaymentSystem\PaymentField', 'id', 'field_id');
    }
}