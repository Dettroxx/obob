<?php

namespace App\Pages\Faq;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model 
{

    protected $table = 'faqs';
    public $timestamps = true;
    protected $fillable = array('category_id', 'name', 'alias', 'content');
    protected $with = array('faq_category');

    public function faq_category() {
        return $this->belongsTo(FaqCategory::class, 'category_id');    
    }
}