<?php

namespace App\Pages\Faq;

use Illuminate\Database\Eloquent\Model;
use App\Pages\Faq\Faq;

class FaqCategory extends Model 
{

    protected $table = 'faq_categories';
    public $timestamps = true;
    protected $fillable = array('lang', 'name', 'alias', 'order');


    public function faqs() {
        return $this->hasMany(Faq::class, 'category_id');
    }
}