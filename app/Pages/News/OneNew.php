<?php

namespace App\Pages\News;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OneNew extends Model 
{

    protected $table = 'news';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('lang', 'name', 'alias', 'introtext', 'content');

}