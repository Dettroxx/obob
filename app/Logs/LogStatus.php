<?php

namespace App\Logs;

use Illuminate\Database\Eloquent\Model;

class LogStatus extends Model 
{

    protected $table = 'log_statuses';
    public $timestamps = false;
    protected $fillable = array('name');

}