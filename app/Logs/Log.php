<?php

namespace App\Logs;

use Illuminate\Database\Eloquent\Model;

class Log extends Model 
{

    protected $table = 'logs';
    public $timestamps = true;
    protected $fillable = array('owner_type', 'owner_id', 'status', 'data');

}