<?php

namespace App\Admin\Controllers\PaymentSystem;

use App\PaymentSystem\Wallet;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use DB;

class ReserveController extends Controller
{
    use ModelForm;

    public function index()
    {
        //Базовая выборка 
        $this->allPayments = $this->allPayments(); 
        return Admin::content(function (Content $content) {
            $content->header('Резерв');
            $content->body($this->grid());
        });
    }

    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Detail');
            $content->description('description');

            $content->body(Admin::show(Wallet::findOrFail($id), function (Show $show) {

                $show->id();

                $show->created_at();
                $show->updated_at();
            }));
        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Create');
            $content->description('description');

            $content->body($this->form());
        });
    }

    protected function grid()
    {
        global $self;
        $self = $this;

        return Admin::grid(Wallet::class, function (Grid $grid) {
            if (!isset($_REQUEST['_scope_'])) {
                $grid->model()->where('id', '>', 9999999999999);
            }
            

            //Специальные ограничения для добавления информации по полям только для данного кошелька
            if (isset($_REQUEST['_scope_'])) {
                $grid->column('name', 'Наименование')->display(function($name) {
                    $code = $this->code;
                    return $code.'<br>'.$name;
                });
                $grid->column('reserve', 'Резерв')->editable();
                $grid->column('max_limit', 'Максимальный лимит');
            } else {
                $grid->tools(function ($tools) {
                    $tools->append('<div class="btn-group pull-right" style="margin-right: 10px; margin-top: 5px">
                        Сначала выберите нужный сервис через контекстное меню фильтра
                    </div>');
                });

                $grid->disablePagination();
                $grid->disableActions();
            }

            //Убираем отображение
            $grid->disableExport(); 
            $grid->disableRowSelector();
            $grid->disableCreation();
            $grid->disableActions();

            //Ненужные элементы 
            $grid->actions(function ($actions) {
                $actions->disableView();
                $actions->disableDelete();
            }); 

            //Настройки фильтра
            $grid->filter(function($filter){ 
                $filter->disableIdFilter();
                $payments = $this->allPayments;
                foreach ($payments as $key => $value) {
                    $filter->scope($key, $value)->where('payment_system_id', "=", $key);
                }
            });
        });
    } 

    protected function form()
    {
        return Admin::form(Wallet::class, function (Form $form) {
            $form->display('id', 'ID');
            $form->number('reserve', 'Резерв')->default(0);
        });
    }

    //Список доступных сервисов
    protected $allPayments = [];
    protected function allPayments() {
        $payments = (array) DB::table("payment_systems")
        ->orderBy('name', 'asc')
        ->pluck("name", "id")
        ->toArray();
        return $payments;
    }


    //Возвращает список резерва
    protected function apiReserve() {
        $result = (array) DB::table("payment_systems as PaymentSystem")
        ->select(
            "PaymentSystem.id as id",
            "PaymentSystem.name as name",
            "PaymentSystem.icon as icon",
            "PaymentSystem.abbr as abbr",
            DB::raw("SUM(Wallet.reserve) as reserve")
        )
        ->join("wallets as Wallet", "PaymentSystem.id", "=", "Wallet.payment_system_id")
        ->where("PaymentSystem.active", "=", true) 
        ->where("PaymentSystem.out", "=", true) 
        ->groupBy("PaymentSystem.id")
        ->orderBy("PaymentSystem.name", "asc")
        ->get()
        ->toArray();

        return $result;
    }
}
