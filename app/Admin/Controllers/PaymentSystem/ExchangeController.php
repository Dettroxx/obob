<?php

namespace App\Admin\Controllers\PaymentSystem;

use App\PaymentSystem\PaymentLink;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use DB;
use View;
use App\PaymentSystem\PaymentSystem;
use Illuminate\Http\Request;
use App\Guests\Guest;
use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Auth\Database\HasPermission;
use Hash;
use App\Bids\BidFieldValue;
use App\Bids\Bid; 

class ExchangeController extends Controller
{
    use ModelForm;

    public function index()
    {
        $this->allPayments = $this->allPayments(); 
        $this->allPaymentsFull = $this->allPaymentsFull(); 

        return Admin::content(function (Content $content) {
            $content->header('Курс обмена');
            $content->body($this->grid());

            //View для таблицы
            $view = View::make('exchange_table', $this->getCurrentTable());
            $data = $view->render();
            $content->body($data);

            //Общий view, который отвечает за все действия
            $view = View::make('exchange_controller');
            $data = $view->render();
            $content->body($data);
        });
    }

    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Detail');
            $content->description('description');

            $content->body(Admin::show(PaymentLink::findOrFail($id), function (Show $show) {

                $show->id();

                $show->created_at();
                $show->updated_at();
            }));
        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $this->current_id = $id;
            $content->header('Редактировать');
            $content->body($this->form()->edit($id));
        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Добавить');
            $content->body($this->form());
        });
    }

    protected function isExist($from, $to) {
        $count = PaymentLink::where("from", "=", $from)
        ->where("to", "=", $to)
        ->count();

        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    protected $coin_types = [
        'coin' => 'Coin',
        'pc' => 'РС',
        'bank' => 'Банк'
    ];

    //Получаем текущий объект
    protected function current_object() {
        if (!isset($this->current_id)) return [];

        $id = $this->current_id;
        $data = (array) PaymentLink::where("id", "=", $id)->with("from_payment")->with("to_payment")->first()->toArray();
        return $data;
    }

    //Формирует таблицу, которую мы используем
    protected function getCurrentTable() {
        //Список валют
        $result_payments = (array) PaymentSystem
        ::orderBy("name", "asc")
        ->get()
        ->toArray();
        
        $payments = array();
        foreach ($result_payments as $payment) {
            $id = $payment["id"];
            $payments[$id] = $payment; 
        }

        //Список связей с ними
        $result_links = (array) PaymentLink
        ::orderBy("id", "asc")
        ->get()
        ->toArray(); 

        $links = array();
        foreach ($result_links as $item) {
            $from = $item["from"];
            $to = $item["to"];
            $links[$from][$to] = $item;
        }

        //Формируем табличные данные
        $body = array();
        $count = count($payments);
        foreach ($payments as $i => $row) {
            $row = array();
            $row["first"] = $payments[$i];

            foreach ($payments as $j => $col) {
                $row["items"][] = $j;
            }
            $body[] = $row;
        }

        $data = array(
            "head" => $payments,
            "body" => $body,
            "links" => $links
        );

        return $data;
    }

    public function test() {
        $elements = $this->getCurrentTable();
        echo "<pre>";
        print_r($elements);
        echo "</pre>";
    }

    protected function grid()
    {
        global $self;
        $self = $this;

        return Admin::grid(PaymentLink::class, function (Grid $grid) { 
            $grid->model()->with('from_payment')->with('to_payment');

            //Убираем отображение
            $grid->disableExport(); 
            $grid->disableRowSelector();
            $grid->disablePagination();

            //Убираем возможность создания если уже выбран хость один элемент
            $data = $_REQUEST;
            if (!isset($data['to']) || empty($data['to']) || !isset($data['from']) || empty($data['from'])) {
                $grid->tools(function ($tools) {
                    $tools->append('<div class="btn-group" style="margin-left: 10px;">
                        Выберите нужное пересечение в таблице
                    </div>');
                });

                $grid->disableActions(); 
            } else {
                $to = $data['to'];
                $from = $data['from'];
                $isexist = $this->isExist($from, $to);
                if ($isexist) {
                    $grid->column('exchange_labels', 'Обмен')->display(function($payment) {
                        $view = View::make('exchange.exchange_labels', [
                            'item' => $this
                        ]);
                        $data = $view->render();
                        return $data;
                    }); 

                    $grid->column('exchange_list', 'Рейтинг обменников')->display(function($payment) {
                        $view = View::make('exchange.exchange_list', [
                            'item' => $this
                        ]);
                        $data = $view->render();
                        return $data;
                    }); 

                    $grid->column('exchange_course', 'Курсы валют')->display(function($payment) {
                        $view = View::make('exchange.exchange_course', [
                            'item' => $this
                        ]);
                        $data = $view->render();
                        return $data;
                    }); 
                } else { 
                    $grid->disableActions();

                    $raw_url = $_SERVER['REQUEST_URI'];
                    $raw_url = preg_replace('/\?.*/', '', $raw_url);
                    $this->parsed_url = $raw_url.'/create?&from='.$from.'&to='.$to;

                    $grid->tools(function ($tools) {
                        global $self;
                        $tools->append('<div class="btn-group" style="margin-right: 10px">
                            <a href="'.$self->parsed_url.'" class="btn btn-sm btn-success">
                                <i class="fa fa-save"></i>&nbsp;&nbsp;Добавить
                            </a>
                        </div>');
                        $tools->append('<div class="btn-group" style="margin-right: 10px; margin-top: 5px">
                            Создайте новую связь
                        </div>');
                    });
                }
            }

            $grid->disableCreation();

            //Ненужные элементы 
            $grid->actions(function ($actions) {
                $actions->disableView();
            }); 

            //Настройки фильтра
            $grid->filter(function($filter) {
                $filter->disableIdFilter();
                $filter->equal('from', 'Откуда')->select($this->allPayments);
                $filter->equal('to', 'Куда')->select($this->allPayments);
            });
        });
    }

    protected $states = [
        'Да'  => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
        'Нет' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
    ];

    protected function form()
    {
        global $self;
        $self = $this;

        return Admin::form(PaymentLink::class, function (Form $form) {
            $data = $_REQUEST;
            $usePayments = false;
            if (isset($data['from']) && isset($data['to'])) {
                $from = $data['from'];
                $to = $data['to'];

                //Данные
                $all = $this->allPaymentsFull();
                $from_payment = $all[$from];
                $to_payment = $all[$to];

                $form->hidden('from')->value($from);
                $form->hidden('to')->value($to);
                $usePayments = true;
            } else {
                global $self;
                $data = $self->current_object();

                if (!empty($data)) {
                    $from_payment = $data['from_payment'];
                    $to_payment = $data['to_payment'];
                    $usePayments = true;
                } 

                $form->hidden('from');
                $form->hidden('to');
            }

            //Фикс для момента удаления
            if ($usePayments) {
                if ($from_payment['icon']) {
                    $from_icon = '<img src="/admin_data/'.$from_payment['icon'].'" style="border-radius: 50%;" width="25px" height="25px"> ';
                } else {
                    $from_icon = "";
                }
    
                if ($to_payment['icon']) {
                    $to_icon = '<img src="/admin_data/'.$to_payment['icon'].'" style="border-radius: 50%;" width="25px" height="25px"> ';
                } else {
                    $to_icon = ""; 
                }
    
                $form->html('
                    <div class="box box-solid box-default no-margin">
                        <div class="box-body">
                            '.$from_icon.$from_payment['name'].' <b>(откуда)</b>
                        </div>
                    </div>
                ');
                
    
                $form->html('
                    <div class="box box-solid box-default no-margin">
                        <div class="box-body">
                            '.$to_icon.$to_payment['name'].' <b>(куда)</b>
                        </div>
                    </div>
                ');
            }

            $form->divider();
            $form->switch('active', 'Активен')->states($this->states);
            $form->switch('alert', 'Включить оповещение')->states($this->states);
            $form->divider();
            $form->switch('reverse', 'Реверсный коэффициент')->states($this->states);
            $form->number('course', 'Курс')->rules('required');
            $form->number('extra', 'Наша наценка, %')->rules('required');
            $form->divider();
            $form->number('corridor_from', 'Коридор (От, %)')->rules('required');
            $form->number('corridor_to', 'Коридор (До, %)')->rules('required');
            $form->switch('corridor_active', 'Включить коридор')->states($this->states);

            $form->disableReset();
        });
    }


    protected $allPayments = [];
    protected function allPayments() {
        $payments = (array) DB::table("payment_systems")
        ->orderBy('name', 'asc')
        ->pluck("name", "id")
        ->toArray();
        return $payments;
    }

    protected $allPaymentsFull = [];
    protected function allPaymentsFull() {
        $payments = (array) DB::table("payment_systems")
        ->orderBy('name', 'asc')
        ->get()
        ->toArray();

        $result = array();
        foreach ($payments as $payment) {
            $id = $payment->id;
            $result[$id] = (array) $payment;
        }

        return $result;
    }


    //Создает новую заявку
    protected function apiCreateBid(Request $request) {
        $all = $request->all();

        //return $all;
        $data = $this->prepareBidData($all);

        //Попытка создать нового гостя
        $data_guest = $this->createGetGuest($data['email'], $data['name']); 

        //Дополнительная обработка данных о пользователе
        $this->touchGuest($data_guest);

        //Создаем новую заявку
        $bid = $this->createBid($data_guest, $data);

        //Возвращаем данные
        return [
            'success' => true,
            'bid' => $bid
        ];   
    }

    //Формирует данные
    protected function prepareBidData($all) {
        //Базовые поля
        $result['name'] = $all['data']['name'];
        $result['email'] = $all['data']['email'];
        $result['from_id'] = $all['from_current']['id']; 
        $result['to_id'] = $all['to_current']['id'];
        $result['from_value'] = $all['from_value'];
        $result['to_value'] = $all['to_value'];
 
        //Обработка дополнительных полей
        $result['from_fields'] = $this->prepareBidFields($all['data']['fields']['from_fields']);
        $result['to_fields'] = $this->prepareBidFields($all['data']['fields']['to_fields']);

        return $result;
    }

    //Обработка полей
    protected function prepareBidFields($fields) {
        $result = [];
        foreach ($fields as $item) {
            $id = $item['id'];
            $value = $item['value'];
            $result[] = array(
                'id' => $id,
                'value' => $value
            );
        }

        return $result;
    }

    //Создание гостя
    protected function createGetGuest($email, $name) {
        $guest = Guest::where('email', '=', $email)->first();
        $data = array(
            'email' => $email,
            'password' => null,
        );

        //Создание нового пользователя
        if (empty($guest)) {
            $data = $this->createGuest($email, $name);
            $data['new'] = true;
        } else {
            $admin = Administrator::where("id", "=", $guest->id)->first();
            $data['admin'] = $admin; 
            $data['guest'] = $guest;
            $data['new'] = false;
        }

        return $data;
    }

    //Создать гостя
    protected function createGuest($email, $name) {
        //Создание нового пользователя
        $password = $this->getPassword($email);
        $admin = new Administrator();
        $admin->name = $name;
        $admin->username = $email;
        $admin->password = Hash::make($password); 
        $admin->save();

        //Создание связи
        DB::table('admin_role_users')
        ->insert([
            'role_id' => 2, //Роль пользователя
            'user_id' => $admin->id
        ]); 

        //Создание нового гостя
        $guest = new Guest();
        $guest->email = $email;
        $guest->name = $name;
        $guest->id = $admin->id;
        $guest->save();

        //Формирование данных
        $result_data = [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'admin' => $admin,
            'guest' => $guest
        ];

        //Отправка данных о новосозданном пользователе
        $this->sendUserInfo($result_data);

        //Возвращение данных
        return $result_data;
    }


    //Генерация пароля
    protected function getPassword($email) {
        $time = time();
        $total = $email.$time;
        $md5 = md5($total);
        $pass = substr($md5, 0, 12);
        return $pass;
    }


    //Обновляет данные о пользователе
    protected function touchGuest($data) {
        $r_data = request()->get('data');
        $name = $r_data['name'];
        $admin = $data['admin'];
        $admin->name = $name;
        $admin->touch(); 
        $guest = $data['guest'];
        $guest->ip = request()->ip();
        $guest->name = $name;
        $guest->touch();
    }


    //Отправляет данные о новосозданном пользовате
    protected function sendUserInfo($data) {

    }


    //Создает новую заявку
    protected function createBid($data_guest, $data) { 
        $bid = $this->createBidObject($data_guest, $data);
        $this->createFieldsValues($data, $bid->id);
        return $bid;
    }


    //Создает новый список значений полей
    protected function createFieldsValues($data, $bidId) {
        $fields_prepared = array();
        if (isset($data['from_fields']) && !empty($data['from_fields'])) { 
            foreach ($data['from_fields'] as $field) {
                $fields_prepared[] = [
                    "field_id" => $field["id"],
                    "value" => $field["value"],
                    "out_in" => 0
                ];
            }
        }
        if (isset($data['to_fields']) && !empty($data['to_fields'])) { 
            foreach ($data['to_fields'] as $field) {
                $fields_prepared[] = [
                    "field_id" => $field["id"],
                    "value" => $field["value"],
                    "out_in" => 1
                ];
            }
        }

        //Создаем список значений
        $result_ids = array();
        foreach ($fields_prepared as $item) {
            $obj = new BidFieldValue();
            $obj->fill($item);
            $obj->bid = $bidId;
            $obj->save();
        }
    }


    //Создает заявку 
    protected function createBidObject($data_guest, $data) {
        $result = array();
        $result['from'] = $data['from_id'];
        $result['to'] = $data['to_id'];
        $result['operator'] = null;
        $result['guest_id'] = $data_guest['guest']->id;
        $result['status'] = 1;
        $result['value_from'] = $data['from_value'];
        $result['value_to'] = $this->CalculateValue($data['from_id'], $data['to_id'], $data['from_value'], $data['to_value']);
        $result['trash'] = false;

        //Создаем заявку
        $bid = new Bid();
        $bid->fill($result);
        $bid->save();

        //Возвращаем id созданной заявки
        return $bid;
    }


    //Рассчитывает конечное значение value
    protected function CalculateValue($from, $to, $from_value, $to_value) { //TODO: Доделать, надо сделать правильный рассчет данных
        return $to_value;
    }        


    //Производит связывание заявки с полями
    protected function linkBidAndFields($valuesIds, $bidId) {
        $result = array();
        foreach ($valuesIds as $id) {
            $insert = [
                "bid_id" => $bidId,
                "value_id" => $id
            ];
            $result[] = $insert;
        }

        DB::table("bid_fields_links")->insert($result);
    }
}
