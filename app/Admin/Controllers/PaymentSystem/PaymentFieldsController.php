<?php

namespace App\Admin\Controllers\PaymentSystem;

use App\PaymentSystem\PaymentField;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use DB;

class PaymentFieldsController extends Controller
{
    use ModelForm;

    public function index()
    {
        //Базовая выборка
        $this->allPayments = $this->allPayments();

        return Admin::content(function (Content $content) {
            $content->header('Дополнительные поля сервисов');
            $content->body($this->grid());
        });
    }

    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Detail');
            $content->description('description');

            $content->body(Admin::show(PaymentField::findOrFail($id), function (Show $show) {
                $show->id();
                $show->created_at();
                $show->updated_at();
            }));
        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Редактировать дополнительное поле сервиса');
            $content->body($this->form()->edit($id));
        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('Добавить дополнительное поле сервиса');
            $content->body($this->form());
        });
    }

    protected function grid()
    {
        global $self;
        $self = $this;
        return Admin::grid(PaymentField::class, function (Grid $grid) {
            if (!isset($_REQUEST['_scope_'])) {
                $grid->model()->where('id', '>', 9999999999999);
            }

            //Специальные ограничения для добавления информации по полям только для данного кошелька
            if (isset($_REQUEST['_scope_'])) {
                $grid->column('payment_system_id', 'Сервис')->display(function($id) {
                    global $self;
                    $items = $self->allPayments;
                    if (isset($items[$id])) return $items[$id];
                    return $id;
                });
    
                $grid->column('key', 'Наименование на англ.');
                $grid->column('info', 'Название поля');
                $grid->column('required', 'Обязательное')->switch($this->states)->editable()->sortable();
                $grid->column('show_out', 'При продаже')->switch($this->states)->editable()->sortable();
                $grid->column('show_in', 'При покупке')->switch($this->states)->editable()->sortable();
                $grid->column('show_wallet', 'В кошельке')->switch($this->states)->editable()->sortable();
            } else {
                $grid->tools(function ($tools) {
                    $tools->append('<div class="btn-group pull-right" style="margin-right: 10px; margin-top: 5px">
                        Сначала выберите нужный сервис через контекстное меню фильтра
                    </div>');
                });
                $grid->disablePagination();
                $grid->disableActions();
                $grid->disableCreation();
            }

            //Убираем отображение
            $grid->disableFilter();
            $grid->disableExport(); 
            $grid->disableRowSelector();

            //Ненужные элементы
            $grid->actions(function ($actions) {
                $actions->disableView();
            });

            //Настройки фильтра
            $grid->filter(function($filter){
                $filter->disableIdFilter();
                $payments = $this->allPayments;
                foreach ($payments as $key => $value) {
                    $filter->scope('payment_'.$key, $value)->where('payment_system_id', "=", $key);
                }
            });
        });
    }

    protected $states = [
        'Да'  => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
        'Нет' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
    ];

    protected function form()
    {
        return Admin::form(PaymentField::class, function (Form $form) {
            $form->select('payment_system_id', 'Сервис')->options($this->allPayments())->rules('required');
            $form->text('key', 'Наименование на англ.')->rules('required');
            $form->text('info', 'Название поля')->rules('required');
            $form->icon('icon', 'Иконка')->rules('required');
            $form->text('mask', 'Маска ввода')->placeholder('R 0000 0000 0000 и т.д.');
            $form->switch('required', 'Обязательное')->states($this->states);
            $form->switch('show_out', 'При продаже')->states($this->states);
            $form->switch('show_in', 'При покупке')->states($this->states);
            $form->switch('show_wallet', 'В кошельке')->states($this->states);

            //Убираем
            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableView(); 
            });
            $form->disableReset();
        });
    }

    //Список всех доступных языков
    protected $allPayments = [];
    protected function allPayments() {
        $payments = (array) DB::table("payment_systems")
        ->orderBy('name', 'asc')
        ->pluck("name", "id")
        ->toArray(); 
        return $payments;
    }


    //Дополнительные поля
    public function apiFields($type = 0, $id = 0) {
        $elements = DB::table('payment_fields')
        ->select("*", DB::raw('NULL as value'))
        ->where('payment_system_id', '=', $id);

        //Проверка
        if ($type == 'from') {
            $elements = $elements->where("show_in", "=", true);
        } else if ($type == 'to') {
            $elements = $elements->where("show_out", "=", true); 
        }

        $elements = $elements
        ->get();

        return $elements;
    }
}
