<?php

namespace App\Admin\Controllers\PaymentSystem;

use App\PaymentSystem\Wallet;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use DB;
use View;

class WalletController extends Controller
{
    use ModelForm;

    public function index()
    {
        //Базовая выборка 
        $this->allPayments = $this->allPayments(); 

        return Admin::content(function (Content $content) {
            $content->header('Список кошельков');
            $content->body($this->grid());
        });
    }

    public function edit($id)
    {
        $this->current_id = $id;
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Редактировать кошелек'); 
            $content->body($this->form_edit()->edit($id));
        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('Добавить кошелек'); 
            $content->body($this->form());
        });
    }

    protected function grid()
    {
        global $self;
        $self = $this;

        return Admin::grid(Wallet::class, function (Grid $grid) {
            if (!isset($_REQUEST['_scope_'])) {
                $grid->model()->where('id', '>', 9999999999999);
            }

            //Специальные ограничения для добавления информации по полям только для данного кошелька
            if (isset($_REQUEST['_scope_'])) {
                $raw_url = $_SERVER['REQUEST_URI'];
                $raw_url = preg_replace('/\?.*/', '', $raw_url);
                $this->parsed_url = $raw_url.'/create?&payment_system_id='.$_REQUEST['_scope_'];

                $grid->tools(function ($tools) {
                    global $self;
                    $tools->append('<div class="btn-group pull-right" style="margin-right: 10px">
                        <a href="'.$self->parsed_url.'" class="btn btn-sm btn-success">
                            <i class="fa fa-save"></i>&nbsp;&nbsp;Добавить
                        </a>
                    </div>');
                });

                //Название кошелька
                $grid->column('name', 'Наименование')->display(function($name) {
                    $code = $this->code;
                    return $code.'<br>'.$name;
                });
                $grid->column('active', 'Активен')->display(function($name) {
                    if ($name) {
                        return 'Да';
                    } else {
                        return 'Нет';
                    }
                });

                $grid->column('id', 'Дополнительные поля')->display(function($id) {
                    global $self;
                    $items = $self->getActualFields($id);
                    $view = View::make('wallet_fields', ['items' => $items]);
                    $contents = (string) $view;
                    return $contents;
                });

                $grid->column('reserve', 'Резерв');
                $grid->column('max_limit', 'Максимальный лимит');
            } else {
                $grid->tools(function ($tools) {
                    $tools->append('<div class="btn-group pull-right" style="margin-right: 10px; margin-top: 5px">
                        Сначала выберите нужный сервис через контекстное меню фильтра
                    </div>');
                });
                $grid->disablePagination();
                $grid->disableActions();
            }

            //Убираем отображение
            $grid->disableExport(); 
            $grid->disableRowSelector();
            $grid->disableCreation();
            

            //Ненужные элементы 
            $grid->actions(function ($actions) {
                $actions->disableView();
            }); 

            //Настройки фильтра
            $grid->filter(function($filter){ 
                $filter->disableIdFilter();
                $payments = $this->allPayments;
                foreach ($payments as $key => $value) {
                    $filter->scope($key, $value)->where('payment_system_id', "=", $key);
                }
            });
        });
    }

    protected $states = [
        'Да'  => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
        'Нет' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
    ];

    //Получает актуальный список полей для конкретной платежной системы
    protected function getActualFields($id = 0) {
        $wallet = Wallet::where("id", "=", $id)->first();
        if (!$wallet) {
            $payment_id = $this->current_payment;
        } else {
            $payment_id = $wallet->payment_system_id;   
        }

        //Получаем список
        $items = (array) DB::table('payment_systems as Payment')
        ->join('payment_fields as Field', 'Payment.id', '=', 'Field.payment_system_id')
        ->where('Payment.id', '=', $payment_id)
        ->where('Field.show_wallet', '=', true)
        ->orderBy('Field.id')
        ->get()
        ->toArray(); 

        //Получаем список существующих полей
        if ($wallet) {
            $fields = $wallet->fields;
        } else {
            $fields = array();
        }

        //Проходимся по списку полей 
        $result_items = array();
        foreach ($items as $element) {
            $item = (array) $element;
            $key = $item['key'];

            //Значение
            $value = null;
            if (isset($fields[$key])) {
                $value = $fields[$key];
            }

            //Заголовок
            $title = $key;
            if (isset($item['info']) && !empty($item['info'])) {
                $title = $item['info'];
            }

            //Обязательное
            $required = false;
            if (isset($item['required'])) {
                $required = (boolean) $item['required'];
            }

            //Выполняем заполнение
            $result_items[] = array(
                "key" => $key,
                "title" => $title,
                "required" => $required,
                "value" => $value
            );
        }

        return $result_items;
    }

    //Базовые поля формы
    protected function base_form($form) {
        $form->text('code', 'Код кошелька/сервиса')->rules('required');
        $form->text('name', 'ФИО/Название кошелька')->rules('required');
        $form->number('reserve', 'Резерв')->default(0)->rules('required');
        $form->number('max_limit', 'Максимальный лимит')->rules('required');
        $form->switch('active', 'Активен')->states($this->states);

        //Рендер дополнительных полей
        if (isset($this->current_id)) {
            $current_id = $this->current_id;
            $fields = $this->getActualFields($current_id);
            if (!empty($fields)) {
                $this->current_fields = $fields;
                $form->embeds('fields', 'Дополнительные поля', function ($form) {
                    $fields = $this->current_fields;
                    $this->render_fields($fields, $form);
                });
            }
        } else {
            $fields = $this->getActualFields();
            if (!empty($fields)) {
                $this->current_fields = $fields;
                $form->embeds('fields', 'Дополнительные поля', function ($form) {
                    $fields = $this->current_fields;
                    $this->render_fields($fields, $form);
                });
            }
        }
    }

    //Рендеринг формы
    protected function render_fields($fields, $form) {
        foreach ($fields as $field) {
            if ($field['required']) {
                $form->text($field['key'], $field['title'])->placeholder(' ')->rules('required');
            } else {
                $form->text($field['key'], $field['title'])->placeholder(' ');
            }
        }
    }

    protected function form()
    {
        return Admin::form(Wallet::class, function (Form $form) {
            if (isset($_REQUEST['payment_system_id'])) {
                $payment_service_id = (int) $_REQUEST['payment_system_id'];
                $allPayments = $this->allPayments();
                $name = $allPayments[$payment_service_id];
                $form->html('<p style="margin-bottom: -8px;">Кошелек для сервиса<br><b>'.$name.'</b></p>');
                $form->hidden('payment_system_id')->value($payment_service_id);  
                $form->divider(); 

                $this->current_payment = $payment_service_id;
                $this->base_form($form);
            }

            //Убираем
            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableView(); 
            });
            $form->disableReset();
        });
    }

    protected function form_edit()
    {
        return Admin::form(Wallet::class, function (Form $form) { 
            $form->hidden('payment_system_id');  
            $this->base_form($form);

            //Убираем
            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableView(); 
            });
            $form->disableReset();
        });
    }

    //Список доступных сервисов
    protected $allPayments = [];
    protected function allPayments() {
        $payments = (array) DB::table("payment_systems")
        ->orderBy('name', 'asc')
        ->pluck("name", "id")
        ->toArray();
        return $payments;
    }
}
