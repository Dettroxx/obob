<?php

namespace App\Admin\Controllers\PaymentSystem;

use App\PaymentSystem\PaymentSystem;
use App\PaymentSystem\PaymentLink;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use DB;

class PaymentSystemController extends Controller
{
    use ModelForm;

    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Платежные сервисы');
            $content->body($this->grid());
        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Edit');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('Добавить платежный сервис');
            $content->body($this->form());
        });
    }

    protected function grid()
    {
        return Admin::grid(PaymentSystem::class, function (Grid $grid) {
            $grid->column("icon", "Иконка")->image(config('app.storage'), 24, 24);
            $grid->column('name', 'Наименование');
            $grid->column('active', 'Активен')->switch($this->states)->editable()->sortable();
            $grid->column('referal_out', 'Реферальный вывод')->switch($this->states)->editable()->sortable();
            $grid->column('out', 'Продажа')->switch($this->states)->editable()->sortable();
            $grid->column('in', 'Покупка')->switch($this->states)->editable()->sortable();

            //Убираем отображение
            $grid->disableFilter();
            $grid->disableExport(); 
            $grid->disableRowSelector();

            //Ненужные элементы
            $grid->actions(function ($actions) {
                $actions->disableView();
            });

            //Настройки фильтра
            $grid->filter(function($filter){
                $filter->disableIdFilter();
                $types = $this->types;
                foreach ($types as $key => $value) {
                    $filter->scope('lang_'.$key, $value)->where('type', "=", $key);
                }
            });
        });
    }

    protected $states = [
        'Да'  => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
        'Нет' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
    ];


    //Типы платежных сервисов
    protected $types = array(
        'bank' => 'Банковский сервис',
        'pc' => 'Платежная система',
        'coin' => 'Криптовалютный сервис'
    );

    protected function form()
    {
        return Admin::form(PaymentSystem::class, function (Form $form) {
            $form->image('icon', 'Иконка')->rules('required');
            $form->text('name', 'Наименование')->rules('required');
            $form->select('type', 'Тип сервиса')->options($this->types)->rules('required');
            $form->switch('active', 'Активен')->states($this->states);
            $form->switch('referal_out', 'Реферальный вывод')->states($this->states);
            $form->switch('out', 'Продажа')->states($this->states);
            $form->switch('in', 'Покупка')->states($this->states);
            $form->number('precise', 'Точность (знаки после запятой)')->rules('required')->value(2);
            $form->text('abbr', 'Сокращение'); 

            //Убираем
            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableView(); 
            });
            $form->disableReset();
        });
    }

   
    //Общий метод для получения данных
    protected function apiGet(Request $request) {
        $direction = $request->get('direction');
        switch ($direction) {
            case 'from':
                return $this->apiGetFrom($request);

            case 'to':
                return $this->apiGetTo($request);
        }

        
    }

    //Если это список "от"
    protected function apiGetFrom(Request $request) {
        $payments = (array) DB::table("payment_systems as Payment")
        ->select("Payment.*")
        ->join('payment_links as FromLink', "Payment.id", "=", "FromLink.from")
        ->where('Payment.active', '=', true)
        ->where('Payment.out', '=', true)
        ->orderBy('Payment.name', 'asc')
        ->get()
        ->toArray()
        ; 

        return $payments;
    }

    //Если это список "после"
    protected function apiGetTo(Request $request) {
        $from = $request->get("from");
        if (empty($from)) return;

        $payments = (array) DB::table("payment_systems as Payment")
        ->select("Payment.*")
        ->join('payment_links as ToLink', "Payment.id", "=", "ToLink.to")
        ->where('ToLink.from', '=', $from)
        ->where('Payment.active', '=', true)
        ->where('Payment.out', '=', true)
        ->orderBy('Payment.name', 'asc')
        ->get()
        ->toArray()
        ; 

        return $payments;
    }


    //Функция, производящая конвертирования
    protected function convert(Request $request) { 
        $value = $request->get("value");
        if ($value <= 0) return;

        $direction = $request->get('direction');
        $before = $request->get("before");
        $after = $request->get("after");

        switch ($direction) {
            case 'from':
                if ($after == $before) return 0;
                return $this->convertFromTo($before, $after, $value);

            case 'to':
                return $this->convertToFrom($before, $after, $value);
        }
    }

    //Рассчет слева-направо
    public function convertFromTo($before, $after, $value) {
        $link = PaymentLink::where("from", "=", $before)
        ->where("to", "=", $after)
        ->first();

        $value = (double) $value;
        $course = (double) $link->course;
        $extra = (double) $link->extra;
        $reverse = (boolean) $link->reverse;

        return $this->convertCalculateFromTo($reverse, $course, $extra, $value);
    }

    //Функция для рассчета курса Слева-Направо
    protected function convertCalculateFromTo($reverse, $course, $extra, $value) {
        if (!$reverse) {
            $big = (double) $course * (1 + ($extra / 100));
            $result = $value / $big;
        } else {
            $big = $course * (1 - ($extra / 100));
            $result = $value * $big; 
        }
        return $result;
    }

    //Рассчет справа-налево
    public function convertToFrom($before, $after, $value) {
        $link = PaymentLink::where("from", "=", $after)
        ->where("to", "=", $before)
        ->first();

        $value = (double) $value;
        $course = (double) $link->course;
        $extra = (double) $link->extra;
        $reverse = (boolean) $link->reverse;

        return $this->convertCalculateToFrom($reverse, $course, $extra, $value);
    }

    //Функция для рассчета курса Справа-Налево
    protected function convertCalculateToFrom($reverse, $course, $extra, $value) {
        if (!$reverse) {
            $big = (double) $course * (1 + ($extra / 100));
            $result = $value * $big; 
        } else {
            $big = $value / (1 - ($extra / 100));
            $result = $big / $course; 
        }
        return $result;
    }

    //Для препарирования курса
    protected function prepareCourseLink($link) {
        if (!$link->reverse) {
            $val = $this->convertCalculateToFrom($link->reverse, $link->course, $link->extra, 1);
            return [
                "from" => $val,
                "to" => 1
            ];
        } else {
            $val = $this->convertCalculateFromTo($link->reverse, $link->course, $link->extra, 1);
            return [
                "from" => 1,
                "to" => $val
            ];
        }
    }

    //Информация
    protected function apiData(Request $request) {
        $from_id = $request->get("from");
        $to_id = $request->get("to");

        //Получаем данные
        $link = PaymentLink::where("from", "=", $from_id)
        ->where("to", "=", $to_id)
        ->first();
        if (empty($link)) return array();

        //Возвращаемые данные
        $data = array();
        $data["link"] = $link->toArray();
        $data["link"]["prepared"] = $this->prepareCourseLink($link);

        return $data;
    }
}
