<?php

namespace App\Admin\Controllers\Guests;

use App\Guests\Guest;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Guests\GuestReferalLink;
use DB;
use View;

class GuestController extends Controller
{
    use ModelForm;

    public function index()
    {
        $this->allReferers = $this->allReferers(); 
        return Admin::content(function (Content $content) {
            $content->header('Пользователи');
            $content->body($this->grid());

            //Рендер дополнительных данные
            $item = array();
            $item['referers'] = $this->makeEditableList($this->allReferers);
            $view = View::make('guest_table_common', [
                'item' => $item
            ]);
            $data = $view->render();
            $content->body($data);
        });
    }

    //Функция, которая преобразует в редактируемый список
    protected function makeEditableList($items) {
        $result = array();
        foreach ($items as $key => $value) {
            $result[] = [
                "value" => $key,
                "text" => $value
            ];
        }
        return $result;
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Редактировать пользователя');
            $content->body($this->form()->edit($id));
        });
    } 

    protected $states = [
        'Да'  => ['value' => 1, 'text' => 'Да', 'color' => 'success'],
        'Нет' => ['value' => 0, 'text' => 'Нет', 'color' => 'danger'],
    ];

    protected function grid()
    {
        return Admin::grid(Guest::class, function (Grid $grid) {
            if (isset($_REQUEST['_scope_'])) {
                $model = $grid->model();
                $scope = $_REQUEST["_scope_"];
                switch ($scope) {
                    case 'created_at': //По дате регистрации
                        $model->orderBy("created_at", "desc");
                        break;

                    case 'email': //По электронной почте
                        $model->orderBy("email", "asc");
                        break;
                }
            }

            $grid->column('display_field', 'Список пользователей')->display(function($id) {
                $view = View::make('guest_table', ['item' => $this]);
                return $view->render();
            });

            //Настройка
            $grid->disableExport(); 
            $grid->disableRowSelector();
            $grid->disableCreation();
            $grid->disableActions();
            $grid->expandFilter();

            //Фильтр элементов
            $grid->filter(function($filter){ 
                $filter->disableIdFilter();
                $filter->scope('created_at', 'Сортировать: по дате регистрации');
                $filter->scope('email', 'Сортировать: по электронной почте');
                $filter->scope('online', 'Сортировать: по статусу "online"');
                $filter->scope('bids', 'Сортировать: По количеству заявок');
                $filter->scope('in_money', 'Сортировать: По принятой суммы денег');
                $filter->scope('out_money', 'Сортировать: По отданной сумме денег');
                $filter->scope('referals', 'Сортировать: По количеству рефералов');
                $filter->scope('referals_money', 'Сортировать: По реферальным начислениям');
            });

            //Ненужные элементы 
            $grid->actions(function ($actions) {
                $actions->disableView();
                $actions->disableDelete();
            }); 
        });
    }

    protected function form()
    {
        return Admin::form(Guest::class, function (Form $form) {
            $form->number('earning_percent', '% заработка');
            $form->number('remuneration_percent', '% вознаграждения');
            $form->select('referer.from', 'Пригласитель')->options($this->allReferers()); 
            $form->switch('is_shop', 'Магазин?')->options($this->states);
            $form->number('shop_percent', 'Магазин % вывода');
            $form->switch('alerts', 'Уведомлять')->options($this->states);
        });
    }

    protected $allReferers = [];
    protected function allReferers() {
        $referers = (array) DB::table("guests")
        ->orderBy('email', 'asc')
        ->pluck("email", "id")
        ->toArray();
        return $referers;
    }
}
