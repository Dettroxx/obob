<?php

namespace App\Admin\Controllers\Utils;

use App\Utils\Language;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Intervention\Image\ImageManager;

class LanguagesController extends Controller
{
    use ModelForm;
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Список языков');
            $content->body($this->grid());
        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Редактировать язык');
            $content->body($this->form()->edit($id));
        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('Добавить язык');
            $content->body($this->form());
        });
    }

    protected function grid()
    {
        global $self;
        $self = $this;
        return Admin::grid(Language::class, function (Grid $grid) {
            $grid->model()->orderBy('key', 'asc');

            $grid->column('icon', 'Иконка')->display(function($id) {
                return '<img src="/admin_data/'.$id.'" width="25" height="25" style="border-radius: 50%;"/>';
            });
            $grid->column('key', 'Код');
            $grid->column('name', 'Наименование');

            //Убираем отображение
            $grid->disableFilter();
            $grid->disableExport();
            $grid->disableRowSelector();

            //Ненужные элементы
            $grid->actions(function ($actions) {
                $actions->disableView();
            });
        });
    }

    protected function form()
    {
        return Admin::form(Language::class, function (Form $form) {
            $form->image('icon', 'Иконка (указать путь)')->rules('required');
            $form->text('key', 'Код (из двух букв англ.)')->rules('required');
            $form->text('name', 'Наименование')->rules('required');

            //Убираем
            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableView(); 
            });
            $form->disableReset();
        });
    }
}
