<?php

namespace App\Admin\Controllers\Bids;

use App\Bids\Bid;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class FreezeBidController extends BidController {
    protected $index_name = 'Замороженные заявки';
    protected $bid_view = 'bids.freeze_bid';

    //Статус по - умолчанию
    protected $status_id = 4;

    protected function whereCurrent($model) {
        $model->where("status", "=", 4); //Замороженная заявка
    }
}
