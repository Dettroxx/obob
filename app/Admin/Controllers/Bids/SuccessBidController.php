<?php

namespace App\Admin\Controllers\Bids;

use App\Bids\Bid;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class SuccessBidController extends BidController {
    protected $index_name = 'Обработанные заявки';
    protected $bid_view = 'bids.success_bid';

    //Статус по - умолчанию
    protected $status_id = 2;

    protected function whereCurrent($model) {
        $model->where("status", "=", 2); //Обработанная заявка
    }
}
