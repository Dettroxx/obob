<?php

namespace App\Admin\Controllers\Bids;

use App\Bids\Bid;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class WaitBidController extends BidController {
    protected $index_name = 'Предварительные заявки';
    protected $bid_view = 'bids.wait_bid';

    //Статус по - умолчанию
    protected $status_id = 5;

    protected function whereCurrent($model) { 
        $model->where("status", "=", 5); //Предварительная заявка
    }
}
