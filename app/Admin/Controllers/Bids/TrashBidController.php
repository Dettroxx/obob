<?php

namespace App\Admin\Controllers\Bids;

use App\Bids\Bid;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class TrashBidController extends BidController {
    protected $index_name = 'Удаленные заявки';
    protected $bid_view = 'bids.trash_bid';
    protected function whereCurrent($model) {
        $model->where('trash', '=', true); 
    }
}
