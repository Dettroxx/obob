<?php

namespace App\Admin\Controllers\Bids;

use App\Bids\Bid;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use View;
use Auth;
use DB;

class BidController extends Controller
{
    use ModelForm;

    //Переопределяемые элементы
    protected $index_name = 'Index';
    protected $detail_name = 'Detail';
    protected $edit_name = 'Edit';
    protected $create_name = 'Create';

    //Статус по - умолчанию
    protected $status_id = 1;

    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header($this->index_name);
            $content->body($this->grid());

            //Общие скрипты для всех заявок
            $view = View::make('bids.common');
            $data = $view->render();
            $content->body($data);
        });
    }

    public function show($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header($this->detail_name);
            $content->body(Admin::show(Bid::findOrFail($id), function (Show $show) {
                $show->id();
                $show->created_at();
                $show->updated_at();
            }));
        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header($this->edit_name);
            $content->body($this->form()->edit($id));
        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header($this->create_name);
            $content->body($this->form());
        });
    } 

    protected function whereCurrent($model) {
        $model->where("trash", "=", false);
    }

    protected $bid_view = 'bids.bid_layout';
    protected function grid()
    {
        global $self;
        $self = $this;

        return Admin::grid(Bid::class, function (Grid $grid) {
            $this->whereCurrent($grid->model());
            $grid->column('bid_row', 'Информация о заявке')->display(function($payment) {
                global $self;
                $view = View::make($self->bid_view, [
                    'item' => $this,
                    'operator' => Admin::user()->toArray()
                ]);
                $data = $view->render();
                return $data;
            }); 

            //Настройка
            $grid->disableExport(); 
            $grid->disableRowSelector();
            $grid->disableCreation();
            $grid->disableActions();

            //Ненужные элементы 
            $grid->actions(function ($actions) {
                $actions->disableView();
                $actions->disableDelete();
            }); 

            //Настройка фильтрации
            $grid->filter(function($filter){
                $filter->disableIdFilter();

                //ID заявки
                $filter->equal('id', 'ID Заявки')->inputmask([
                    'regex' => '[0-9]*'
                ], $icon = 'hashtag');

                //Email
                $filter->where(function ($query) {
                    $query->whereHas('guest_data', function ($query) {
                        $query->where('email', 'like', "%{$this->input}%");
                    });
                }, 'Email');

                //IP - адрес
                $filter->where(function ($query) {
                    $query->whereHas('guest_data', function ($query) {
                        $query->where('ip', 'like', "%{$this->input}%");
                    });
                }, 'IP - адрес');

                //Дата создания
                $filter->between('created_at', "Дата создания")->datetime([
                    'format' => 'DD.MM.YYYY HH:mm',
                ]);

                //Дата обновления
                $filter->between('updated_at', "Дата обработки")->datetime([
                    'format' => 'DD.MM.YYYY HH:mm',
                ]);

                //С какой платежной системы
                $filter->equal('from', 'Из какой ПС')->select($this->PaymentsGet('from'));

                //Сколько отдают
                $filter->between('value_from', 'Сколько отдают'); 

                //На какую платежную систему
                $filter->equal('to', 'На какую ПС')->select($this->PaymentsGet('to'));

                //Сколько получают
                $filter->between('value_to', 'Сколько получают'); 

                //Дополнительное поле (откуда)
                $filter->where(function ($query) {
                    $query->whereHas('from_fields', function ($query) {
                        $query->where('field_id', '=', $this->input);
                    });
                }, 'Дополнительное поле (откуда)')->select($this->getFieldsFrom(false)); 

                //Дополнительное поле (значение)
                $filter->where(function ($query) {
                    $query->whereHas('from_fields', function ($query) {
                        $query->where('value', 'like', "%{$this->input}%");
                    });
                }, 'Значение (откуда)');
                
                //Дополнительное поле (куда)
                $filter->where(function ($query) {
                    $query->whereHas('to_fields', function ($query) {
                        $query->where('field_id', '=', $this->input);
                    });
                }, 'Дополнительное поле (куда)')->select($this->getFieldsFrom(true)); 

                //Дополнительное поле (значение)
                $filter->where(function ($query) {
                    $query->whereHas('from_fields', function ($query) {
                        $query->where('value', 'like', "%{$this->input}%");
                    });
                }, 'Значение (куда)');
            });
        });
    }

    //Возвращает список PaymentSystem (откуда)
    protected function PaymentsGet($field) {
        return DB::table("bids as Bid")
        ->where("status", "=", $this->status_id)
        ->join("payment_systems as PaymentSystem", "Bid.".$field, "=", "PaymentSystem.id")
        ->groupBy("PaymentSystem.id")
        ->orderBy("PaymentSystem.name", "asc")
        ->pluck('PaymentSystem.name', 'PaymentSystem.id');
    }

    //Список полей
    protected function getFieldsFrom($outin = false) { 
        $fields = DB::table("bids as Bid")
        ->select(DB::raw(
            "CONCAT(PaymentSystem.name, '. ', PaymentField.info) as namefield"
        ), 'PaymentField.id as id')
        ->where('Bid.status', "=", $this->status_id)
        ->join('bid_field_values as BidFieldValue', 'Bid.id', '=', 'BidFieldValue.bid')
        ->join('payment_fields as PaymentField', 'BidFieldValue.field_id', '=', 'PaymentField.id')
        ->join('payment_systems as PaymentSystem', 'PaymentField.payment_system_id', '=', 'PaymentSystem.id')
        ->where('BidFieldValue.out_in', '=', $outin)
        ->pluck('namefield', 'id');
        return $fields;
    } 

    protected function form()
    {
        return Admin::form(Bid::class, function (Form $form) {
            $form->hidden('status');
            $form->hidden('trash'); 
            $form->hidden('operator'); 
        });
    }

    public function test() {
        $data = Bid::first(); 
        echo "<pre>";
        print_r($data->toArray());
        echo "</pre>";
    }
}
