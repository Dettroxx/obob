<?php

namespace App\Admin\Controllers\Bids;

use App\Bids\Bid;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class NewBidController extends BidController {
    protected $index_name = 'Новые заявки';
    protected $bid_view = 'bids.new_bid';
    protected function whereCurrent($model) {
        $model->where("status", "=", 1);
        parent::whereCurrent($model);
    }
}
