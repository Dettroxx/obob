<?php

namespace App\Admin\Controllers\Pages;

use App\Pages\Page;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ContactsPageController extends PagesController
{
    protected $index_name = 'Контакты';
    protected $edit_name = 'Редактировать страницу "Контакты"';
    protected $create_name = 'Добавить страницу "Контакты"';
    protected $family = 'contacts';
}
