<?php

namespace App\Admin\Controllers\Pages;

use App\Pages\Page;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class RulePageController extends PagesController
{
    protected $index_name = 'Правила обмена';
    protected $edit_name = 'Редактировать правила обмена';
    protected $create_name = 'Добавить правило обмена';
    protected $family = 'rules';
}
