<?php

namespace App\Admin\Controllers\Pages\News;

use App\Pages\News\OneNew;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use DB;

class NewsController extends Controller
{
    use ModelForm;

    public function index()
    {
        //Базовая выборка
        $this->allLangs = $this->allLangs();

        return Admin::content(function (Content $content) {

            $content->header('Список новостей');
            $content->body($this->grid());
        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редактировать новость');
            $content->body($this->form()->edit($id));
        });
    }


    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Добавить новость');
            $content->body($this->form());
        });
    }

    protected function grid()
    {
        global $self;
        $self = $this;
        return Admin::grid(OneNew::class, function (Grid $grid) {
            $grid->model()->orderBy('created_at', 'desc'); 

            $grid->column('lang', 'Язык')->display(function($id) {
                global $self;
                $items = $self->allLangs;
                if (isset($items[$id])) return $items[$id];
                return $id;
            });
            $grid->column('name', 'Наименование');
            $grid->column('introtext', 'Краткое описание');
            $grid->column('created_at', 'Дата публикации');


            //Убираем отображение
            $grid->disableExport(); 
            $grid->disableRowSelector();

            //Ненужные элементы
            $grid->actions(function ($actions) {
                $actions->disableView();
            });

            //Настройки фильтра
            $grid->filter(function($filter){
                $filter->disableIdFilter();
                $langs = $this->allLangs;
                foreach ($langs as $key => $value) {
                    $filter->scope('lang_'.$key, $value)->where('lang', "=", $key);
                }
            });
        });
    }

    protected function form()
    {
        return Admin::form(OneNew::class, function (Form $form) {
            $form->select('lang', 'Язык')->options($this->allLangs())->rules('required');
            $form->text('name', 'Название')->rules('required');
            $form->text('alias', 'Алиас');
            $form->textarea('introtext', 'Краткое описание');
            $form->editor('content', 'Содержание')->rules('required');
 
            //Убираем
            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableView(); 
            });
            $form->disableReset();
        });
    }

    //Список всех доступных языков
    protected $allLangs = [];
    protected function allLangs() {
        $langs = (array) DB::table("languages")
        ->pluck("name", "id")
        ->toArray();
        return $langs;
    }
}
