<?php

namespace App\Admin\Controllers\Pages\Faq;

use App\Pages\Faq\Faq;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use DB;

class FaqController extends Controller
{
    use ModelForm;

    public function index()
    {
        //Базовая выборка
        $this->allCategories = $this->allCategories();

        return Admin::content(function (Content $content) {
            $content->header('Список вопросов (FAQ)');
            $content->body($this->grid());
        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Редактировать вопрос (FAQ)');
            $content->body($this->form()->edit($id));
        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Добавить вопрос (FAQ)');
            $content->body($this->form());
        });
    }

    protected function grid()
    {
        global $self;
        $self = $this;
        return Admin::grid(Faq::class, function (Grid $grid) {
            $grid->column('category_id', 'Категория')->display(function($id) {
                global $self;
                $items = $self->allCategories;
                if (isset($items[$id])) return $items[$id];
                return $id;
            }); 

            $grid->column('name', 'Наименование')->sortable();
            $grid->column('alias', 'Алиас');
            $grid->column('order', 'Порядок')->sortable();


            //Убираем отображение
            $grid->disableExport(); 
            $grid->disableRowSelector();

            //Настройки фильтра
            $grid->filter(function($filter){
                $filter->expand();
                $filter->disableIdFilter();
                $filter->equal('category_id', 'Категория')->select($this->allCategories);
            });

            //Ненужные элементы
            $grid->actions(function ($actions) {
                $actions->disableView();
            });
        });
    }

    protected function form()
    {
        return Admin::form(Faq::class, function (Form $form) {
            $form->select('category_id', 'Категория')->options($this->allCategories())->rules('required');
            $form->text('name', 'Наименование')->rules('required');
            $form->text('alias', 'Алиас');
            $form->number('order', 'Порядок');
            $form->editor('content', 'Содержание')->rules('required');

            //Убираем
            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableView(); 
            });
            $form->disableReset();
        });
    }

    //Список всех доступных языков
    protected $allCategories = [];
    protected function allCategories() {
        $categories = (array) DB::table("faq_categories")
        ->pluck("name", "id")
        ->toArray();
        return $categories;
    }
}
