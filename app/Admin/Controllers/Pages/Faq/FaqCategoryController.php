<?php

namespace App\Admin\Controllers\Pages\Faq;

use App\Pages\Faq\FaqCategory;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use DB;

class FaqCategoryController extends Controller
{
    use ModelForm;

    public function index()
    {
        //Базовая выборка
        $this->allLangs = $this->allLangs();

        return Admin::content(function (Content $content) {
            $content->header('Список категорий FAQ');
            $content->body($this->grid());
        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Редактировать категорию FAQ');
            $content->body($this->form()->edit($id));
        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('Добавить категорию FAQ');
            $content->body($this->form());
        });
    }

    protected function grid()
    {
        global $self;
        $self = $this;
        return Admin::grid(FaqCategory::class, function (Grid $grid) {
            if (!isset($_REQUEST['_scope_'])) {
                $grid->model()->where('lang', '=', 1);
            }
            

            $grid->column('lang', 'Язык')->display(function($id) {
                global $self;
                $items = $self->allLangs;
                if (isset($items[$id])) return $items[$id];
                return $id;
            });
            $grid->column('name', 'Наименование');
            $grid->column('order', 'Сортировка');

            //Убираем отображение
            $grid->disableFilter();
            $grid->disableExport(); 
            $grid->disableRowSelector();

            //Ненужные элементы
            $grid->actions(function ($actions) {
                $actions->disableView();
            });

            //Настройки фильтра
            $grid->filter(function($filter){
                $filter->disableIdFilter();
                $langs = $this->allLangs;
                foreach ($langs as $key => $value) {
                    $filter->scope('lang_'.$key, $value)->where('lang', "=", $key);
                }
            });
        });
    }

    protected function form()
    {
        return Admin::form(FaqCategory::class, function (Form $form) {
            $form->select('lang', 'Язык')->options($this->allLangs())->rules('required');
            $form->text('name', 'Наименование')->rules('required');
            $form->number('order', 'Порядок сортировки');

            //Убираем
            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableView(); 
            });
            $form->disableReset();
        });
    }

    //Список всех доступных языков
    protected $allLangs = [];
    protected function allLangs() {
        $langs = (array) DB::table("languages")
        ->pluck("name", "id")
        ->toArray();
        return $langs;
    }
}
