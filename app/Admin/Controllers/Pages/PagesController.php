<?php

namespace App\Admin\Controllers\Pages;

use App\Pages\Page;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use DB;

class PagesController extends Controller
{
    use ModelForm;

    protected $index_name = 'Index';
    public function index()
    {
        //Базовая выборка
        $this->allLangs = $this->allLangs();
        return Admin::content(function (Content $content) {
            $content->header($this->index_name);
            $content->body($this->grid());
        });
    }

    protected $edit_name = 'Edit';
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header($this->edit_name);
            $content->body($this->form()->edit($id));
        });
    }

    protected $create_name = 'Create';
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header($this->create_name);
            $content->body($this->form());
        });
    }

    protected function page_grid($grid) {
        global $self;
        $self = $this;

        $grid->column('lang', 'Язык')->display(function($id) {
            global $self;
            $items = $self->allLangs;
            if (isset($items[$id])) return $items[$id];
            return $id;
        });
        $grid->column('name', 'Наименование');
        $grid->column('alias', 'Алиас');
    }

    protected function grid()
    {
        return Admin::grid(Page::class, function (Grid $grid) {
            $grid->model()->where('family', '=', $this->family);
            $this->page_grid($grid);

            //Убираем отображение
            $grid->disableFilter();
            $grid->disableExport(); 
            $grid->disableRowSelector();

            //Ненужные элементы
            $grid->actions(function ($actions) {
                $actions->disableView();
            });

            //Настройки фильтра
            $grid->filter(function($filter){
                $filter->disableIdFilter();
                $langs = $this->allLangs;
                foreach ($langs as $key => $value) {
                    $filter->scope('lang_'.$key, $value)->where('lang', "=", $key);
                }
            });
        });
    }

    protected $family = '';
    protected function form()
    {
        return Admin::form(Page::class, function (Form $form) {
            $form->hidden('family')->value($this->family);
            $form->hidden('alias')->value($this->family);

            $form->select('lang', 'Язык')->options($this->allLangs())->rules('required');
            $form->text('name', 'Наименование')->rules('required');
            $form->editor('content', 'Содержание')->rules('required');

            //Убираем
            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableView(); 
            });
            $form->disableReset();
        });
    }

    //Список всех доступных языков
    protected $allLangs = [];
    protected function allLangs() {
        $langs = (array) DB::table("languages")
        ->pluck("name", "id")
        ->toArray();
        return $langs;
    }
}
