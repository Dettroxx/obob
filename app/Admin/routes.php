<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');

    //Заявки
    Route::group([
        'prefix'        => 'bids',
    ], function (Router $router) {
        $router->resource('new', \Bids\NewBidController::class); 
        $router->resource('success', \Bids\SuccessBidController::class); 
        $router->resource('error', \Bids\ErrorBidController::class); 
        $router->resource('trash', \Bids\TrashBidController::class); 
        $router->resource('freeze', \Bids\FreezeBidController::class); 
        $router->resource('wait', \Bids\WaitBidController::class); 
    }); 

    //Управление
    Route::group([
        'prefix'        => 'control',
    ], function (Router $router) {
        $router->resource('payments', \PaymentSystem\PaymentSystemController::class); 
        $router->resource('payments-fields', \PaymentSystem\PaymentFieldsController::class); 
        $router->resource('wallets', \PaymentSystem\WalletController::class); 
        $router->resource('reserve', \PaymentSystem\ReserveController::class); 
        $router->resource('guests', \Guests\GuestController::class);
        $router->resource('cource', \PaymentSystem\ExchangeController::class);
    }); 

    //Страницы
    Route::group([
        'prefix'        => 'pages',
    ], function (Router $router) {
        $router->resource('faq', \Pages\Faq\FaqController::class); 
        $router->resource('news', \Pages\News\NewsController::class); 

        //Статичные ресурсы
        $router->resource('rules', \Pages\RulePageController::class); 
        $router->resource('partners', \Pages\PartnersPageController::class); 
        $router->resource('contacts', \Pages\ContactsPageController::class); 
    }); 

    //Настройки
    Route::group([
        'prefix'        => 'settings',
    ], function (Router $router) {
        $router->resource('languages', \Utils\LanguagesController::class);
        $router->resource('faq-categories', \Pages\Faq\FaqCategoryController::class);
    });    
});
