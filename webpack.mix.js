let mix = require('laravel-mix');
mix.js('resources/assets/js/vue.js', 'public/js')
.sass('resources/assets/sass/app.scss', 'public/css');
