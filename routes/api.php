<?php

use Illuminate\Http\Request;
use Illuminate\Routing\Router;

//Платежные системы
Route::group([
    'as' => 'api.payments.',
    'prefix' => 'payments',
    'namespace' => 'App\Admin\Controllers\PaymentSystem'
], function (Router $router) {
    $router->get('get', 'PaymentSystemController@apiGet')->name('get');
    $router->get('data', 'PaymentSystemController@apiData')->name('data');
    $router->post('convert', 'PaymentSystemController@convert')->name('convert'); 

    //Дополнительные поля для заполнения
    $router->get('fields/{type?}/{id?}', 'PaymentFieldsController@apiFields')->name('fields');

    //Отправка запроса на создания
    $router->post('create', 'ExchangeController@apiCreateBid')->name('create_bid');
});

//Резерв
Route::group([
    'as' => 'api.reserve.',
    'prefix' => 'reserve',
    'namespace' => 'App\Admin\Controllers\PaymentSystem'
], function (Router $router) {
    $router->get('get', 'ReserveController@apiReserve')->name('get');
});