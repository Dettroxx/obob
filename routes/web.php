<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index');
Route::get('/rules', 'PublicController@rules');
Route::get('/partners', 'PublicController@partners');
Route::get('/news', 'PublicController@news');
Route::get('/news/{id}', 'PublicController@new_one');
Route::get('/faq', 'PublicController@faq');
Route::get('/faq/{id}', 'PublicController@faq_one');
Route::get('/contacts', 'PublicController@contacts');

Route::get('/test', '\App\Admin\Controllers\Bids\BidController@getFieldsFrom');