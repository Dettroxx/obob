<script>
    var bid_pk = 0;
    var bid_name = '';
    var bid_value = '';

    //Функция для обработка
    function handleStatus(restore) {
        if (bid_name != 'status') return;

        //Элементы
        var text = '';
        var type = '';
        var color = '';

        switch (bid_value) {
            case '1': //Если в новые заявки
                text = 'Переместить в новые заявки?';
                type = 'info';
                color = '#3c8dbc';
                break;

            case '2': //Если в обработанные заявки
                text = 'Переместить в обработанные заявки?';
                type = 'success';
                color = '#008d4c';
                break;

            case '3': //Если в ошибочные заявки
                text = 'Переместить в ошибочные заявки?';
                type = 'warning';
                color = '#e08e0b';
                break;

            case '4': //Если в замороженные заявки
                text = 'Переместить в замороженные заявки?';
                type = 'info';
                color = '#269abc';
                break;

            case '5': //Если в предварительные заявки
                text = 'Переместить в предварительные заявки?';
                type = 'info';
                color = '#605ca8';
                break;

            case '666': //Если необходимо удалить элемент
                text = 'Переместить в удаленные заявки?';
                type = 'error'; 
                color = '#d73925'; 
                break;
        }
 
        swal({
            title: text,
            type: type,
            showCancelButton: true,
            confirmButtonColor: color,
            confirmButtonText: "Подтвердить",
            closeOnConfirm: true,
            cancelButtonText: "Отмена"
        },
        function(){
            var data = {};

            var url = '{{route("new.index")}}/' + bid_pk;
            if (bid_value == '666') { //Если надо удалить
                data = {
                    name: 'trash',
                    value: '1',
                    pk: bid_pk,
                    _editable: 1,
                    _method: 'PUT',
                    _token:LA.token, 
                };
            } else { //Все остальное
                data = {
                    name: 'status',
                    value: bid_value,
                    pk: bid_pk,
                    _editable: 1,
                    _method: 'PUT',
                    _token:LA.token,
                };
            }

            

            //Если это восстановление
            if (restore) {
                data = {
                    name: 'trash',
                    value: '0',
                    pk: bid_pk,
                    _editable: 1,
                    _method: 'PUT',
                    _token:LA.token,
                };
            }

            $.ajax({
                method: 'post',
                url: url,
                data: data,
                success: function (data) {
                    $.pjax.reload('#pjax-container');
                    if (typeof data === 'object') {
                        if (data.status) {
                            toastr.success(data.message);
                        } else {
                            toastr.error(data.message);
                        }
                    }
                }
            });
        });
    }


    //Функция для обработки операторской функции
    function handleOperator() {
        if (bid_name != 'operator') return;
        
        //Информация
        switch (bid_value) {
            case '1': //Если взять заявку в обработку
                text = 'Взять заявку в обработку?';
                type = 'info';
                color = '#3c8dbc';
                break;

            default: //Если не брать заявку в обработку
                text = 'Освободить заявку?';
                type = 'warning';
                color = '#e08e0b';
                break;
        }

        swal({
            title: text,
            type: type,
            showCancelButton: true,
            confirmButtonColor: color,
            confirmButtonText: "Подтвердить",
            closeOnConfirm: true,
            cancelButtonText: "Отмена"
        },
        function(){
            var url = '{{route("new.index")}}/' + bid_pk;
            var data = {
                name: 'operator',
                value: bid_value,
                pk: bid_pk,
                _editable: 1,
                _method: 'PUT',
                _token:LA.token,
            };


            $.ajax({
                method: 'post',
                url: url,
                data: data,
                success: function (data) {
                    $.pjax.reload('#pjax-container');
                    if (typeof data === 'object') {
                        if (data.status) {
                            toastr.success(data.message);
                        } else {
                            toastr.error(data.message);
                        }
                    }
                }
            });
        });
    }


    $(document).ready(function() {
        //Обычные кнопки
        $(".bid-btn").click(function() {
            var current = $(this);
            bid_pk = current.attr('data-pk');
            bid_name = current.attr('data-name');
            bid_value = current.attr('data-value');

            //Обработка
            handleStatus();

            //Обработка смены оператора
            handleOperator();
        });

        //Для восстановления заявки в нужном статусе
        $(".bid-restore").click(function() {
            var current = $(this);
            bid_pk = current.attr('data-pk');
            bid_name = current.attr('data-name');
            bid_value = current.attr('data-value');

            //Обработка смены статусов
            handleStatus(true);
        });


        //Для "перерасчета значений"
        $(".btn-recalculate").click(function() {
            $.pjax.reload('#pjax-container');
            toastr.success("Данные обновлены");
        });

        //Для покаща информации по логам
        $(".btn-logs").click(function() {
            alert("Тут будут показаны логи");
        });
    });
</script>