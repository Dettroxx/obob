@extends('bids.bid_layout')
@section('box_class', 'box-danger') 

@section('bid-buttons')
    Заявка была удалена в статусе:<br/>
    <b>
        <b>{{$item['status_data']['name']}} заявка</b>
    </b>
    <br/><br/>
    <button type="button" data-pk="{{$item['id']}}" data-name="status" data-value="{{$item['status']}}" class="bid-restore btn btn-block btn-danger">Восстановить</button>
@stop