@php
    if (isset($item['updated_at']) && !empty($item['updated_at'])) {
        $date = $item['updated_at']->format('d.m.Y H:i');
    } else if (isset($item['deleted_at']) && !empty($item['deleted_at'])) {
        $date = $item['deleted_at']->format('d.m.Y H:i');
    } else if (isset($item['deleted_at']) && !empty($item['deleted_at'])) {
        $date = $item['created_at']->format('d.m.Y H:i');
    } else {
        $date = 0;
    }

    //Дата взятия в обработку
    use Jenssegers\Date\Date;
    Date::setLocale('ru');
    if ($item['updated_at']) {
        $ost = Date::parse($item['updated_at'])->diffForHumans();
    } else {
        $ost = null;
    }
@endphp

<div class="box bid-box box-solid @yield('box_class')">
    <div class="box-header bid-header">
        <div class="bid-num">
            Заявка: #{{$item['id']}} 
        </div>
        <div class="bid-email">
            IP: {{$item['guest_data']['ip']}}  | 
            <i class="fa fa-fw fa-envelope-o"></i>
            {{$item['guest_data']['email']}} 
        </div>
        <div class="bid-date">
            <i class="fa fa-fw fa-clock-o"></i>
            {{$date}}
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-xs-4">
                <p>Получаем:</p>
                <p class="cash">{{round($item['value_from'], $item['payment_from_data']['precise'])}} {{$item['payment_from_data']['abbr']}}</p>
                <p class="valute-p"><img src="/admin_data/{{$item['payment_from_data']['icon']}}" class="valute-p-img">
                    {{$item['payment_from_data']['name']}}
                </p>
                <div class="table-responsive">
                    <table class="table table-striped bordered-t">
                        <tbody>
                            @foreach ($item['from_fields'] as $field)
                                <tr>
                                    <td width="50%">
                                        <b>
                                            {{$field['field']['info']}}
                                        </b>
                                    </td>
                                    <td width="50%">
                                        {{$field['value']}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-xs-4">
                <p>Отдаем:</p>
                <p class="cash">{{round($item['value_to'], $item['payment_to_data']['precise'])}} {{$item['payment_to_data']['abbr']}}</p>
                <p class="valute-p"><img src="/admin_data/{{$item['payment_to_data']['icon']}}" class="valute-p-img">
                    {{$item['payment_to_data']['name']}}
                </p>
                <div class="table-responsive">
                    <table class="table table-striped bordered-t">
                        <tbody>
                            @foreach ($item['to_fields'] as $field)
                                <tr>
                                    <td width="50%">
                                        <b>
                                            {{$field['field']['info']}}
                                        </b>
                                    </td>
                                    <td width="50%">
                                        {{$field['value']}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="bid-buttons">
                    @section('bid-buttons')
                        @if ($item['operator'])
                            @if ($item['status'] != '1')
                                <button type="button" data-pk="{{$item['id']}}" data-name="status" data-value="1" class="bid-btn btn btn-block btn-primary">В новые</button><br/>
                            @endif
                            @if ($item['status'] != '2')
                                <button type="button" data-pk="{{$item['id']}}" data-name="status" data-value="2" class="bid-btn btn btn-block btn-success">В обработанные</button><br/>
                            @endif
                            @if ($item['status'] != '3')
                                <button type="button" data-pk="{{$item['id']}}" data-name="status" data-value="3" class="bid-btn btn btn-block btn-warning">В ошибочные</button><br/>
                            @endif

                            <button type="button" data-pk="{{$item['id']}}" data-name="status" data-value="666" class="bid-btn btn btn-block btn-danger">В удаленные</button><br/>

                            @if ($item['status'] != '4')
                                <button type="button" data-pk="{{$item['id']}}" data-name="status" data-value="4" class="bid-btn btn btn-block btn-info">В замороженные</button><br/>
                            @endif
                            @if ($item['status'] != '5')
                                <button type="button" data-pk="{{$item['id']}}" data-name="status" data-value="5" class="bid-btn btn btn-block bg-purple">В предварительные</button>
                            @endif 
                        @else 
                            <button type="button" data-pk="{{$item['id']}}" data-name="operator" data-value="{{$operator['id']}}" class="bid-btn btn btn-block btn-primary">Взять в обработку</button><br/>
                        @endif
                    @show
                </div>
            </div>
        </div>
    </div>
    @section('bid-footer-info')
        <div class="box-footer">
            <div class="pull-left">
                @php
                    if (!empty($item['PaymentLink'])) {
                        $extra = round($item['PaymentLink']["extra"], 2);
                        $course = $item['PaymentLink']['course'];

                        //Обрабатываем 
                        if ($item['PaymentLink']['reverse']) {
                            $value_from = $item["value_from"];
                            $value_to = $item["value_to"];
                            $need = $value_from * $course;

                            $rs = $value_from * $course;
                            
                            $plus = $rs - $need;
                            $comm = 0;
                        } else {
                            $value_from = $item["value_from"];
                            $value_to = $item["value_to"];

                            $rs = (double) $value_from / $course;
                            $need = $value_to * $course;
                            $plus = $value_from - $need;
                            $comm = ($value_from - $need) / $need * 100;
                        }
                    }
                @endphp

                Комиссия текущая: <b>{{$extra or "не задано"}}</b> % |
                Комиссия сохраненная: 
                @if ($comm > $extra) 
                    <b><span class="green">{{round($comm, 2)}} %</span></b>
                @else 
                    <b><span class="red">{{round($comm, 2)}} %</span></b>
                @endif
                
                | Доход: 
                @if ($plus > 0) 
                    <b><span class="green">{{$plus}} {{$item['payment_to_data']['abbr']}}</span></b>
                @else 
                    <b><span class="red">{{$plus}} {{$item['payment_to_data']['abbr']}}</span></b>
                @endif
                
                
            </div>
            <div class="pull-right">
                @php
                    $pdc = $item['payment_to_data']['WalletsCount'];
                    $value = $item['payment_to_data']['WalletMax'];
                    $precise = $item['payment_to_data']['precise'];
                    $abbr = $item['payment_to_data']['abbr'];
                    $value = round($value, $precise); 
                @endphp

                @if ($pdc > 0) 
                    Резерв: {{$value}} {{$abbr}}
                @else
                    <span class="red">Для конечного сервиса не создано кошельков!</span> 
                @endif
                
                @if ($ost && $item['operator'])
                    | Взята в обработку {{$ost}}
                @endif
            </div>
        </div>
    @show
    <div class="box-footer footer-panel">
        <div class="text-right">
            @section('bid-footer')
                <button type="button" class="btn-inline btn-logs bid-btn btn btn-block btn-default">Логи</button>
                @section('bid-operator-footer')
                    @if ($item['operator'])
                        <button type="button" data-pk="{{$item['id']}}" data-name="operator" data-value="" class="btn-inline bid-btn btn btn-block btn-default">Освободить</button>
                    @endif

                    <button type="button" class="btn-inline btn-recalculate bid-btn btn btn-block btn-default">Перерасчет</button>
                @show
            @show
        </div>
    </div>
</div>