@if ($items && count($items) > 0) 
    @foreach ($items as $item)
        @if ($item['value'])
            <b>{{$item['title']}}</b>: {{$item['value']}}<br/>
        @endif
    @endforeach
@endif