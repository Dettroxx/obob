<div id="exchange_table" class="exchange_table">
    <div class="box">
        <div class="box-body">
            <table class="table-exchange">
                <tbody>
                    <tr>
                        <td class="autotd"></td>
                        @foreach ($head as $item)
                            <td>
                                <img src="/admin_data/{{$item['icon']}}" width="25px" height="25px"/>
                            </td>
                        @endforeach
                    </tr>

                    @foreach ($body as $item)
                        <tr>
                            <td class="autotd text-left">
                                <img src="/admin_data/{{$item['first']['icon']}}" width="25px" height="25px"/> {{$item['first']['name']}}
                            </td>

                            @foreach ($item['items'] as $element)
                                @php
                                    $from = $item["first"]["id"];
                                    $to = $element;
                                @endphp
                                <td> 
                                    @if ($from == $to)

                                    @else
                                        @php
                                            $currentlink = route('cource.index', array(
                                                'from' => $from,
                                                'to' => $to
                                            ));
                                        @endphp

                                        @if (isset($links[$from]))
                                            @if (isset($links[$from][$to]))
                                                <a href="{{$currentlink}}">
                                                    {{$links[$from][$to]['extra']}}
                                                </a>
                                            @else
                                                <a href="{{$currentlink}}">X</a>
                                            @endif
                                        @else
                                            <a href="{{$currentlink}}">X</a>
                                        @endif
                                    @endif
                                </td>
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>