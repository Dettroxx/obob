@extends('public')
@section('title-h1')
    Faq
@stop

@section('content')
<style>
    .faq-block:first-child h3 {
        margin-top: 0px;
    }
</style>

<div class="col-md-12">
    @php
        $last_item = array_pop($breadcrumbs);
    @endphp
    <ol class="breadcrumb">
        @foreach ($breadcrumbs as $key => $item)
            <li><a href="{{$key}}">{{$item}}</a></li>
        @endforeach
        <li class="active">{{$last_item}}</li>
    </ol>
	<div class="box box-solid">
		<div class="box-body text-left">
            @foreach ($categories as $item)
                <div class="faq-block">
                    <h3>{{$item['name']}}</h3>
                    <p>
                        @foreach ($item['faqs'] as $faq_idx => $faq_item)
                            <a href="/faq/{{$faq_item['id']}}">{{$faq_idx+1}}. {{$faq_item['name']}}</a><br/>
                        @endforeach
                    </p>
                </div>
            @endforeach
		</div>
	</div>
</div>
<div class="clearfix"></div>
@stop