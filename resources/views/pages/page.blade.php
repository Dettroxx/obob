@extends('public')
@section('title-h1')
    {{$page['name']}}
@stop

@section('content')
<div class="col-md-12">
	@php
        $last_item = array_pop($breadcrumbs);
    @endphp
    <ol class="breadcrumb">
        @foreach ($breadcrumbs as $key => $item)
            <li><a href="{{$key}}">{{$item}}</a></li>
        @endforeach
        <li class="active">{{$last_item}}</li>
    </ol>
	<div class="box box-solid">
		<div class="box-body text-left">
			{!! $page['content'] !!}
		</div>
	</div>
</div>
<div class="clearfix"></div>
@stop