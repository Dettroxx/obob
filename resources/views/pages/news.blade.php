@extends('public')
@section('title-h1')
    {{$title}}
@stop

@section('content')
<div class="col-md-12">
@php
        $last_item = array_pop($breadcrumbs);
    @endphp
    <ol class="breadcrumb" style="margin-bottom: 15px">
        @foreach ($breadcrumbs as $key => $item)
            <li><a href="{{$key}}">{{$item}}</a></li>
        @endforeach
        <li class="active">{{$last_item}}</li>
    </ol>
</div>

@foreach($page as $item)
<div class="clearfix">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
                <li class="pull-left header"><i class="fa fa-file"></i> {{$item->name}}</li>
            </ul>
            <div class="tab-content">
                @php
                    $time = null;
                    if ($item->updated_at) {
                        $time = $item->updated_at;
                    } else {
                        $time = $item->created_at;
                    }
                @endphp

                <div>
                    <p class="text-muted">{{date('d.m.Y H:i', strtotime($time))}}</p>
                </div>
                <div class="tab-pane active">
                    {{$item->introtext}}
                </div>
                <div>
                    <br/>
                    <a href="/news/{{$item->id}}" class="btn btn-block btn-primary btn-sm btn-auto">Подробнее</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach

<div>
    <div class="col-md-12">
        {{$page->render()}}
    </div>
    <div class="clearfix"></div>
</div>
@stop