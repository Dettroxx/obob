<style>
    .grid-refresh + div, .grid-refresh {
        display: none !important;
    }

    .form-horizontal .form-group:last-child {
        margin-bottom: 0px;
    }

    #filter-box {
        padding: 0px !important;
        display: none !important;
    }

    #filter-box + div + div.box-footer {
        display: none !important;
    }

    #pjax-container > .content > .row > div > .box > .box-header {
        padding: 0px;
    }
</style>

<script>
    $(document).ready(function() {
        $('.editable-reverse').editable({
            source: [
                {'text': 'Да', 'value': 1},
                {'text': 'Нет', 'value': 0}
            ],
            success: function() {
                $.pjax.reload('#pjax-container');
                toastr.success('Запись успешно обновлена!');
            }
        });

        $('.editable-number').editable({
            success: function() {
                $.pjax.reload('#pjax-container');
                toastr.success('Запись успешно обновлена!');
            }
        });
        $('.editable-checkbox').editable({
            source: [
                {'text': 'Да', 'value': 1},
                {'text': 'Нет', 'value': 0}
            ],
            success: function() {
                $.pjax.reload('#pjax-container');
                toastr.success('Запись успешно обновлена!');
            }
        });
    });
</script>