
@php
    $fulllink = route('guests.index').'/'.$item["id"];
@endphp

<div class="box box-solid box-primary">
    <div class="box-header">
        <i class="fa fa-fw fa-envelope"></i> {{$item['email']}}
    </div>
    <div class="box-body">
        <table class="table table-bordered smalltable">
            <tbody>
                <tr>
                    <td style="width: 300px"> 
                        <b>ID:</b>
                    </td>
                    <td>
                        {{$item['id']}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>IP:</b>
                    </td>
                    <td>
                        {{$item['ip']}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Имя:</b>
                    </td>
                    <td>
                        {{$item['name']}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Активность:</b> 
                    </td>
                    <td>
                        Offline
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Статус:</b> 
                    </td>
                    <td>
                        {{$item['status']}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Количество заявок:</b> 
                    </td>
                    <td>
                        Всего: 1 |
                        Обработано: 0 |
                        Ошибочные: 0 | 
                        Удаленные: 0
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Количество рефералов:</b> 
                    </td>
                    <td>
                        0
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Реферальные начисления Всего:</b> 
                    </td>
                    <td>
                        0 руб. / 0$ / 0.000000 btc.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Реферальные начисления в аккаунте:</b> 
                    </td>
                    <td>
                        0 руб. / 0$ / 0.000000 btc.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Вознаграждения Всего:</b> 
                    </td>
                    <td>
                        0 руб. / 0$ / 0.000000 btc.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Вознаграждения в аккаунте:</b> 
                    </td>
                    <td>
                        0 руб. / 0$ / 0.000000 btc.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Принято:</b> 
                    </td>
                    <td>
                        0 руб. / 0$ / 0.000000 btc.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Отдано:</b> 
                    </td>
                    <td>
                        0 руб. / 0$ / 0.000000 btc.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Процент заработка:</b> 
                    </td>
                    <td>
                        <a href='#' class='editable-number'data-name="earning_percent" data-type='text' data-pk='{{$item["id"]}}' data-url='{{$fulllink}}' data-value='{{$item["earning_percent"] or ""}}'></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Процент вознаграждения:</b> 
                    </td>
                    <td>
                        <a href='#' class='editable-number'data-name="remuneration_percent" data-type='text' data-pk='{{$item["id"]}}' data-url='{{$fulllink}}' data-value='{{$item["remuneration_percent"] or ""}}'></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Пригласитель</b>
                    </td>
                    <td>
                        <a href='#' class='editable-refferer'data-name="referer.from" data-type='select' data-pk='{{$item["id"]}}' data-url='{{$fulllink}}' data-value='{{$item["referer"]["from"] or ""}}'></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Это магазин?</b>
                    </td>
                    <td>
                        <a href='#' class='editable-checkbox'data-name="is_shop" data-type='select' data-pk='{{$item["id"]}}' data-url='{{$fulllink}}' data-value='{{$item["is_shop"] or ""}}'></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Магазин процент вывода:</b> 
                    </td>
                    <td>
                        <a href='#' class='editable-number' data-name="shop_percent" data-type='text' data-pk='{{$item["id"]}}' data-url='{{$fulllink}}' data-value='{{$item["shop_percent"] or ""}}'></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Отправка оповещений</b>
                    </td>
                    <td>
                        <a href='#' class='editable-checkbox'data-name="alerts" data-type='select' data-pk='{{$item["id"]}}' data-url='{{$fulllink}}' data-value='{{$item["alerts"] or ""}}'></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Доход от магазина</b> 
                    </td>
                    <td>
                        0 руб. / 0$ / 0.000000 btc.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Принято</b> 
                    </td>
                    <td>
                        0 руб. / 0$ / 0.000000 btc.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Отдано</b> 
                    </td>
                    <td>
                        0 руб. / 0$ / 0.000000 btc.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>На счете</b> 
                    </td>
                    <td>
                        0 руб. / 0$ / 0.000000 btc.
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>