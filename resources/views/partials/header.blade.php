<header class="main-header">
    <a href="/" class="logo">
        <span class="logo-lg">{!! config('admin.logo', config('admin.name')) !!}</span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
     
            
        <div class="public-title">
            @yield('title-h1')
        </div>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="user user-menu">
                    <a href="/{{config('admin.route.prefix')}}">
                        <i class="fa fa-fw fa-user"></i>
                        Личный кабинет
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</header>