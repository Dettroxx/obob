<script>
    var urls = {};

    //Ссылки для работы со списком платежных систем
    urls.payments = {
        get: "{{route('api.payments.get', [], false)}}",
        convert: "{{route('api.payments.convert', [], false)}}",
        data: "{{route('api.payments.data', [], false)}}",  

        //Дополнительные поля
        fields: "{{route('api.payments.fields', [], false)}}",

        //Создать заявку
        create_bid: "{{route('api.payments.create_bid', [], false)}}",
    }
    
    //Резерв 
    urls.reserve = {
        get: "{{route('api.reserve.get', [], false)}}",
    }

    window.urls = urls;
</script>