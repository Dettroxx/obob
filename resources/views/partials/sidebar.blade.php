<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header">Меню</li>
            <li>
                <a href="/">
                    <i class="fa fa-credit-card"></i>
                    <span>Обменять валюту</span>
                </a>
            </li>
            <li>
                <a href="/rules">
                    <i class="fa fa-gavel"></i>
                    <span>Правила обмена</span>
                </a>
            </li>
            <li>
                <a href="/partners">
                    <i class="fa fa-users"></i>
                    <span>Партнерам</span>
                </a>
            </li>
            <li>
                <a href="/news">
                    <i class="fa fa-indent"></i>
                    <span>Новости</span>
                </a>
            </li>
            <li>
                <a href="/faq">
                    <i class="fa fa-question-circle"></i>
                    <span>Faq</span>
                </a>
            </li>
            <li>
                <a href="/contacts">
                    <i class="fa fa-envelope"></i>
                    <span>Контакты</span>
                </a>
            </li>
        </ul>
    </section>
</aside>

<aside class="main-sidebar right-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header">Резерв</li>
        </ul>

        <div id="payment-stats">
            <payment-stats></payment-stats>
        </div>
    </section>
</aside>