@php
    $fulllink = route('cource.index').'/'.$item['id'];
@endphp

<table class="table table-bordered nomargin">
    <tbody>
        @if ($item['reverse'])
            <tr>
                <td width="80px">
                    <b>
                        Отдаете:
                    </b>
                </td>
                <td>
                    <a href='#' class='editable-number'data-name="course" data-type='text' data-pk='{{$item["id"]}}' data-url='{{$fulllink}}' data-value='{{$item["course"] or ""}}'></a>
                </td>
            </tr>
            <tr>
                <td>
                    <b>
                        Получаете: 
                    </b>
                </td>
                <td>
                    1 {{$item['to_payment']['abbr'] or ""}}
                </td>
            </tr>
        @else
            <tr>
                <td width="80px">
                    <b>
                        Отдаете: 
                    </b>
                </td>
                <td>
                    1 {{$item['from_payment']['abbr'] or ""}}
                </td>
            </tr>
            <tr>
                <td>
                    <b>
                        Получаете: 
                    </b>
                </td>
                <td>
                    <a href='#' class='editable-number'data-name="course" data-type='text' data-pk='{{$item["id"]}}' data-url='{{$fulllink}}' data-value='{{$item["course"] or ""}}'></a>
                </td>
            </tr>
        @endif
    </tbody>
</table> 