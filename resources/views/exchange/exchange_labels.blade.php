@php
    $fulllink = route('cource.index').'/'.$item['id'];
@endphp 

@php
    $cfrom_value = (double) $item['corridor_from'] * ($item["course"] / 100) + $item["course"];
    $cto_value = (double) $item['corridor_to'] * ($item["course"] / 100) + $item["course"];
    if ($item['reverse']) {
        $abbr = $item["from_payment"]["abbr"];
        $precise = $item["from_payment"]["precise"];
    } else {
        $abbr = $item["to_payment"]["abbr"];
        $precise = $item["to_payment"]["precise"];
    }

    $cfrom_value = round($cfrom_value, $precise);
    $cto_value = round($cto_value, $precise);
@endphp
 
<div style="width: 350px;">
    <table class="fulltable tabletwo">
        <tbody>
            <tr>
                <td width="50%" class="text-right">
                    <b>
                        Параметры обмена
                    </b>
                </td>
                <td width="50%">
                    
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    Откуда:
                </td>
                <td>
                    <img src="/admin_data/{{$item['from_payment']['icon']}}" style="border-radius: 50%;" width="16px" height="16px"> {{$item['from_payment']['name']}}
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    Куда:
                </td>
                <td>
                    <img src="/admin_data/{{$item['to_payment']['icon']}}" style="border-radius: 50%;" width="16px" height="16px"> {{$item['to_payment']['name']}}
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    Курс (откуда):
                </td>
                <td>
                    @if (!$item['reverse'])
                        <a href='#' class='editable-number'data-name="course" data-type='text' data-pk='{{$item["id"]}}' data-url='{{$fulllink}}' data-value='{{$item["course"] or ""}}'></a>
                    @else
                        1 {{$item['from_payment']['abbr'] or ""}}
                    @endif
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    Курс (куда):
                </td>
                <td>
                    @if (!$item['reverse'])
                        1 {{$item['to_payment']['abbr'] or ""}}
                    @else
                        <a href='#' class='editable-number'data-name="course" data-type='text' data-pk='{{$item["id"]}}' data-url='{{$fulllink}}' data-value='{{$item["course"] or ""}}'></a>
                    @endif
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    Наша наценка, %:
                </td>
                <td>
                <a href='#' class='editable-number'data-name="extra" data-type='text' data-pk='{{$item["id"]}}' data-url='{{$fulllink}}' data-value='{{$item["extra"] or ""}}'></a>
                </td>
            </tr>

            <tr class="table-devider">
                <td colspan="2"></td>
            </tr>
            <tr>
                <td class="text-right">
                    <b>
                        Пользователь
                    </b>
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    Отдает: 
                </td>
                <td>
                    @php
                        if (!$item['reverse']) {
                            $ourprice = $item["course"] * $item["extra"] / 100;
                            $get_from = (double) $item["course"] + $ourprice;
                        } 
                    @endphp

                    @if (!$item['reverse'])
                        {{$get_from}} {{$item['from_payment']['abbr'] or ""}}
                    @else
                        1 {{$item['from_payment']['abbr'] or ""}}
                    @endif
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    Получает:
                </td>
                <td>
                    @php
                        if ($item['reverse']) {
                            $ourprice = $item["course"] * $item["extra"] / 100;
                            $get_to = (double) $item["course"] - $ourprice;
                        } 
                    @endphp

                    @if (!$item['reverse'])
                        1 {{$item['to_payment']['abbr'] or ""}}
                    @else
                        {{$get_to}} {{$item['to_payment']['abbr'] or ""}}
                    @endif
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    Наш доход: 
                </td>
                <td>
                    {{$ourprice}}
                    @if (!$item['reverse'])
                        {{$item['from_payment']['abbr'] or ""}}
                    @else
                        {{$item['to_payment']['abbr'] or ""}}
                    @endif
                </td>
            </tr>

            <tr class="table-devider">
                <td colspan="2"></td>
            </tr>
            <tr>
                <td class="text-right">
                    <b>
                        Параметры коридора
                    </b>
                </td>
            </tr>

            <tr>
                <td class="text-right">
                    От:
                </td>
                <td>
                    @if ($item['corridor_active'])
                        <a href='#' class='editable-number'data-name="corridor_from" data-type='text' data-pk='{{$item["id"]}}' data-url='{{$fulllink}}' data-value='{{$item["corridor_from"] or ""}}'></a> %
                        - <b>{{$cfrom_value}} {{$abbr}}</b>
                    @endif
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    До:
                </td>
                <td>
                    @if ($item['corridor_active'])
                        <a href='#' class='editable-number'data-name="corridor_to" data-type='text' data-pk='{{$item["id"]}}' data-url='{{$fulllink}}' data-value='{{$item["corridor_to"] or ""}}'></a> %
                        - <b>{{$cto_value}} {{$abbr}}</b>
                    @endif
                </td>
            </tr>

            <tr class="table-devider">
                <td colspan="2"></td>
            </tr>

            <tr>
                <td class="text-right">
                    <b>
                        Настройки
                    </b>
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    Активен:
                </td>
                <td>
                    <a href="#" class="editable-checkbox" data-name="active" data-type="select" data-pk="{{$item['id']}}" data-url="{{$fulllink}}" data-value="{{$item['active']}}"></a>
                </td>
            </tr>

            <tr>
                <td class="text-right">
                    Реверс:
                </td>
                <td>
                    <a href="#" class="editable-reverse" data-name="reverse" data-type="select" data-pk="{{$item['id']}}" data-url="{{$fulllink}}" data-value="{{$item['reverse']}}"></a>
                </td>
            </tr>

            <tr>
                <td class="text-right">
                    Оповещение:
                </td>
                <td>
                    <a href="#" class="editable-checkbox" data-name="alert" data-type="select" data-pk="{{$item['id']}}" data-url="{{$fulllink}}" data-value="{{$item['alert']}}"></a>
                </td>
            </tr>

            <tr>
                <td class="text-right">
                    Коридор:
                </td>
                <td>
                <a href="#" class="editable-checkbox" data-name="corridor_active" data-type="select" data-pk="{{$item['id']}}" data-url="{{$fulllink}}" data-value="{{$item['corridor_active']}}"></a>
                </td>
            </tr>
        </tbody>
    </table>
</div>