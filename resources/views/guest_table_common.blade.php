<script>
    $(document).ready(function() {
        //Просто изменение
        $('.editable-number').editable();

        //Пригласитель
        $('.editable-refferer').editable({
            source: JSON.parse('{!! json_encode($item["referers"]) !!}')
        });

        //Для чекбоксов
        $('.editable-checkbox').editable({
            source: [
                {'text': 'Да', 'value': 1},
                {'text': 'Нет', 'value': 0}
            ]
        });
    });
</script>