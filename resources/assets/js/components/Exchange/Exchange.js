var value_timeout = null;

export default {
    props: [
        'list', 'value', 'current'
    ],

    watch: {
        list: function (value) {
            if (value.length > 0) {
                this.current_item = value[0];
            }
        }
    },

    computed: {
        current_item: {
            get: function() {
                return this.current;
            },

            set: function(value) {
                this.$emit('selected', value);
            }
        },

        value_item: {
            get: function() {
                return this.value;
            },

            set: function(value) {
                this._value = value;
            }
        },

        precise: {
            get: function() {
                if (this.current_item != null) {
                    return this.current_item.precise;
                } else {
                    return 2;
                }
            }
        }
    },

    methods: {
        selectPayment(item) {
            this.current_item = item;
        },

        onValueChange() {
            if (value_timeout == null) {
                value_timeout = setTimeout(this.sendValueEvent, 500);
            }
        },

        sendValueEvent() {
            value_timeout = null;
            this.$emit('changed', this._value);
        },
    },

    data() {
        return {
            data: {
                _value: 0,
            }
        }
    }
}