//Базовая инициализацияв
window._ = require('lodash');
window.Popper = require('popper.js').default;
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token'); 
}

//Основной скрипт начинается здесь
import Vue from 'vue';
import underscore from 'vue-underscore';
import VueNumeric from 'vue-numeric';

Vue.use(underscore);
Vue.use(VueNumeric)

Vue.component('exchange', require('./components/Exchange/Exchange.vue'));
Vue.component('payment-stats', require('./components/PaymentStats/PaymentStats.vue')); 
 
import axios from 'axios';
Vue.prototype.$http = axios; 

//Компонент exchange
if(document.getElementById("exchange-app")){
    const exchange_app = new Vue({  
        el: '#exchange-app',  
    });
}

//Компонент payment-stats
if (document.getElementById("payment-stats")) {
    const payment_stats = new Vue({  
        el: '#payment-stats',  
    });  
}