<?php

use Illuminate\Database\Seeder;

class PaymentSystemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payment_systems')->delete();
        
        \DB::table('payment_systems')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2018-08-20 07:43:25',
                'updated_at' => '2018-09-03 13:32:50',
                'deleted_at' => NULL,
                'icon' => 'images/5f5f35129b9ff1c73e48b02f2ac3c292.png',
                'name' => 'Perfect Money',
                'in' => 1,
                'out' => 1,
                'referal_out' => 1,
                'precise' => 5,
                'abbr' => NULL,
                'active' => 1,
                'type' => 'bank',
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2018-08-20 07:55:23',
                'updated_at' => '2018-10-14 17:06:45',
                'deleted_at' => NULL,
                'icon' => 'images/advcash.png',
                'name' => 'AdvCash RUB',
                'in' => 1,
                'out' => 1,
                'referal_out' => 1,
                'precise' => 2,
                'abbr' => 'руб',
                'active' => 1,
                'type' => 'coin',
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2018-08-20 07:56:08',
                'updated_at' => '2018-10-14 17:06:40',
                'deleted_at' => NULL,
                'icon' => 'images/4bae154ecb6c76a569c24836714f529a.png',
                'name' => 'AdvCash USD',
                'in' => 1,
                'out' => 1,
                'referal_out' => 1,
                'precise' => 2,
                'abbr' => 'usd',
                'active' => 1,
                'type' => 'coin',
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => '2018-08-20 07:56:43',
                'updated_at' => '2018-08-20 07:56:43',
                'deleted_at' => NULL,
                'icon' => 'images/payeer.png',
                'name' => 'Payeer USD',
                'in' => 1,
                'out' => 1,
                'referal_out' => 1,
                'precise' => 2,
                'abbr' => NULL,
                'active' => 1,
                'type' => 'pc',
            ),
        ));
        
        
    }
}