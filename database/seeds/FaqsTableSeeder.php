<?php

use Illuminate\Database\Seeder;

class FaqsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('faqs')->delete();
        
        \DB::table('faqs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2018-08-20 04:58:43',
                'updated_at' => '2018-08-20 04:58:43',
                'category_id' => 2,
                'name' => 'Какая минимальная сумма обмена?',
                'alias' => NULL,
                'order' => 0,
            'content' => '<p>Минимальные суммы обмена следующие (они зависят от выбора валюты, которую Вы обмениваете):<br />
Рубли: 2500<br />
Доллары: 35<br />
Тенге: 14 300<br />
Баты: 1400<br />
Bitcoin: 0.0032<br />
Litecoin: 0.2<br />
Dash: 0.043<br />
Ethereum: 0.039<br />
Ethereum Classic 1.5<br />
Ripple: 25<br />
Monero: 0.13<br />
ZCash: 0.074<br />
<br />
Исключения, при которых нужно ориентироваться на столбик &quot;Получаю&quot;<br />
Bitcoin - 0.007<br />
Ethereum - 0.03<br />
Monero - 0.3<br />
Ripple - 20</p>',
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2018-08-20 05:01:40',
                'updated_at' => '2018-08-20 05:01:40',
                'category_id' => 2,
                'name' => 'Как быстро проходит обмен?',
                'alias' => NULL,
                'order' => 0,
                'content' => '<p>Мы отправляем средства моментально, после получения их от Вас. В среднем обмен занимает&nbsp;<strong>5-20 минут</strong>. Если вы отправляете нам Bitcoin, то мы ждем 2 подтверждения от сети и затем отправляем вам нужные средства. Ввод неверных реквизитов клиентом, при заполнении заявки, может затянуть обмен.</p>',
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2018-08-20 05:02:04',
                'updated_at' => '2018-08-20 05:02:04',
                'category_id' => 3,
                'name' => 'Как сделать перевод через Сбербанк?',
                'alias' => NULL,
                'order' => 0,
                'content' => '<p>1. Войдите в свой аккаунт в системе &laquo;<a href="https://online.sberbank.ru/CSAFront/index.do" target="_blank">Сбербанк ОнЛ@йн</a>&raquo;.<br />
2. Перейдите по ссылке &laquo;Перевести частному лицу&raquo; (она находится в выпадающем меню кнопки &laquo;операции&raquo; справа от той карты, с которой вы хотите произвести оплату).<br />
3. Выберите &laquo;Перевод клиенту Сбербанка&raquo;.<br />
4. На следующей странице введите в форму наши реквизиты и нажмите на кнопку &laquo;Перевести&raquo;.<br />
5. На открывшейся странице сначала нажмите на кнопку &laquo;Подтвердить по SMS&raquo;, после чего на ваш мобильный телефон придет одноразовый пароль для подтверждения транзакции. Введите этот пароль в соответствующее поле и нажмите на кнопку &laquo;Подтвердить&raquo;.<br />
6. Как только вы проделаете все описанные выше операции, ОБЯЗАТЕЛЬНО вернитесь на наш сайт и нажмите на кнопку &laquo;Я оплатил&raquo;, чтобы оператор нашего сервиса мог проверить поступление средств и провести оплату данной заявки.</p>',
            ),
            3 => 
            array (
                'id' => 4,
                'created_at' => '2018-08-20 05:02:21',
                'updated_at' => '2018-08-20 05:02:21',
                'category_id' => 3,
                'name' => 'Как сделать перевод через Альфа Банк?',
                'alias' => NULL,
                'order' => 0,
                'content' => '<p>1. Войдите в свой аккаунт в системе &laquo;<a href="https://click.alfabank.ru/AlfaSign/index.html?authn_try_count=0&amp;contextType=external&amp;username=string&amp;contextValue=%2Foam&amp;password=sercure_string&amp;challenge_url=%2F..%2Fadfform%2Fauth.jspx&amp;request_id=-4780811573409119866&amp;OAM_REQ=&amp;locale=ru_RU&amp;resource_url=https%253A%252F%252Fclick.alfabank.ru%252FALFAIBSR%252F#/" target="_blank">Альфа-Клик</a>&raquo;.<br />
2. Выберите в верхнем меню пункт&laquo;Переводы&raquo; и перейдите по ссылке&laquo;На счет другому клиенту&raquo;.<br />
3. Введите в открывшуюся форму наши реквизиты и нажмите &laquo;Перевести&raquo;.<br />
4. На следующей странице нажмите на кнопку&laquo;Получить SMS&raquo;, после чего на ваш мобильный телефон придет одноразовый пароль для подтверждения транзакции. Введите этот пароль в расположенное ниже поле и нажмите на кнопку &laquo;Подтвердить&raquo;.<br />
5. Как только вы проделаете все описанные выше операции, ОБЯЗАТЕЛЬНО вернитесь на наш сайт и нажмите на кнопку &laquo;Я оплатил&raquo;, чтобы оператор нашего сервиса мог проверить поступление средств и провести оплату данной заявки.</p>',
            ),
            4 => 
            array (
                'id' => 5,
                'created_at' => '2018-08-20 05:02:39',
                'updated_at' => '2018-08-20 05:02:39',
                'category_id' => 4,
                'name' => 'Банк «Авангард»',
                'alias' => NULL,
                'order' => 0,
            'content' => '<p>Авангард банк - универсальный коммерческий банк, который предоставляет все виды банковских услуг, а так же располагает рядом уникальных сервисов и продуктов. За пользование услугой интернет банк плата не взимается, так же есть мобильные приложения которые ускоряет взаимодействие с банком. Они так же бесплатные. В интернет банке возможно осуществлять переводы со счета на счет, с карты на карту. Есть возможно оплачивать услуги компаний прямо из дома. (например: можно пополнить счет телефона или оплатить интернет)<br />
<br />
Вы можете сделать обмен через наш обменник, с помощью карты банка Авангард. На данный момент доступно 78 направлений обмена. &nbsp;</p>',
            ),
            5 => 
            array (
                'id' => 6,
                'created_at' => '2018-08-20 05:02:59',
                'updated_at' => '2018-08-20 05:02:59',
                'category_id' => 4,
                'name' => 'Банк «Русский Стандарт»',
                'alias' => NULL,
                'order' => 0,
                'content' => '<p>Русский стандарт - один из ведущих частных банков, основан в 1999 году. В арсенале банка присутствуют все банковские услуги: начиная от кредитования заканчивая предоставлением вкладом. Интернет банк Русский Стандарт позволяет осуществлять различные операции онлайн. Так же у банка имеются приложения для мобильных платформ: IOS, Android, Windows Phone. Отдельно хочется сказать, что поддержка банка по горячей линии так себе. Будьте готовы к тому, что на Ваш вопрос Вы так и не получите конкретный ответ.</p>',
            ),
        ));
        
        
    }
}