<?php

use Illuminate\Database\Seeder;

class BidsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('bids')->delete();
        
        \DB::table('bids')->insert(array (
            0 => 
            array (
                'id' => 13,
                'from' => 2,
                'to' => 3,
                'operator' => 1,
                'guest_id' => 17,
                'status' => 1,
                'value_from' => '112.000000',
                'value_to' => '1.538567',
                'trash' => 0,
                'created_at' => '2018-10-14 17:38:05',
                'updated_at' => '2018-10-15 06:46:31',
            ),
        ));
        
        
    }
}