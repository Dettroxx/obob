<?php

use Illuminate\Database\Seeder;

class PaymentFieldValuesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payment_field_values')->delete();
        
        \DB::table('payment_field_values')->insert(array (
            0 => 
            array (
                'id' => 35,
                'payment_field' => 1,
                'value' => 'Федоров Сергей Владимирович',
                'out_in' => 0,
            ),
            1 => 
            array (
                'id' => 36,
                'payment_field' => 2,
                'value' => '+1231231231123',
                'out_in' => 0,
            ),
            2 => 
            array (
                'id' => 37,
                'payment_field' => 3,
                'value' => 'Somemail@ya.ru',
                'out_in' => 1,
            ),
        ));
        
        
    }
}