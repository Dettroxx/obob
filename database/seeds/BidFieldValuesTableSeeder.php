<?php

use Illuminate\Database\Seeder;

class BidFieldValuesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('bid_field_values')->delete();
        
        \DB::table('bid_field_values')->insert(array (
            0 => 
            array (
                'id' => 6,
                'bid' => 13,
                'field_id' => 1,
                'value' => 'Иванов Иван Иванович',
                'out_in' => 0,
            ),
            1 => 
            array (
                'id' => 7,
                'bid' => 13,
                'field_id' => 2,
                'value' => '1231231',
                'out_in' => 0,
            ),
            2 => 
            array (
                'id' => 8,
                'bid' => 13,
                'field_id' => 3,
                'value' => '123123123',
                'out_in' => 1,
            ),
        ));
        
        
    }
}