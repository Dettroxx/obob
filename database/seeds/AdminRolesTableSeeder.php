<?php

use Illuminate\Database\Seeder;

class AdminRolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_roles')->delete();
        
        \DB::table('admin_roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Administrator',
                'slug' => 'administrator',
                'created_at' => '2018-08-19 17:01:41',
                'updated_at' => '2018-08-19 17:01:41',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Пользователь',
                'slug' => 'guest',
                'created_at' => '2018-08-26 13:02:27',
                'updated_at' => '2018-08-26 13:02:27',
            ),
        ));
        
        
    }
}