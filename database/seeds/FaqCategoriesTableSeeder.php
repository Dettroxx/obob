<?php

use Illuminate\Database\Seeder;

class FaqCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('faq_categories')->delete();
        
        \DB::table('faq_categories')->insert(array (
            0 => 
            array (
                'id' => 2,
                'created_at' => '2018-08-19 17:57:59',
                'updated_at' => '2018-08-19 17:57:59',
                'lang' => 1,
                'name' => 'Общие вопросы',
                'alias' => NULL,
                'order' => 0,
            ),
            1 => 
            array (
                'id' => 3,
                'created_at' => '2018-08-19 18:14:55',
                'updated_at' => '2018-08-19 18:14:55',
                'lang' => 1,
                'name' => 'Банковские переводы',
                'alias' => NULL,
                'order' => 5,
            ),
            2 => 
            array (
                'id' => 4,
                'created_at' => '2018-08-19 18:15:12',
                'updated_at' => '2018-08-19 18:15:12',
                'lang' => 1,
                'name' => 'Банки и платежные системы',
                'alias' => NULL,
                'order' => 10,
            ),
        ));
        
        
    }
}