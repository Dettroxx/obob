<?php

use Illuminate\Database\Seeder;

class WalletsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('wallets')->delete();
        
        \DB::table('wallets')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2018-08-21 10:27:59',
                'updated_at' => '2018-10-14 14:55:21',
                'deleted_at' => NULL,
                'payment_system_id' => 2,
                'code' => '123123123123123',
                'name' => 'Иванов Иван Иванович',
                'reserve' => '7800065.000000',
                'active' => 1,
                'max_limit' => '0.000000',
                'fields' => '{"fio":"\\u0418\\u0432\\u0430\\u043d\\u043e\\u0432 \\u0418\\u0432\\u0430\\u043d \\u0418\\u0432\\u0430\\u043d\\u043e\\u0432\\u0438\\u0447","phone":"+8981617212"}',
            ),
            1 => 
            array (
                'id' => 3,
                'created_at' => '2018-08-21 13:06:01',
                'updated_at' => '2018-10-14 14:54:41',
                'deleted_at' => NULL,
                'payment_system_id' => 2,
                'code' => '123123',
                'name' => 'Петров Петр Петрович',
                'reserve' => '600.000000',
                'active' => 1,
                'max_limit' => '0.000000',
                'fields' => '{"fio":"\\u041f\\u0435\\u0442\\u0440\\u043e\\u0432","phone":"+12312312123123"}',
            ),
            2 => 
            array (
                'id' => 4,
                'created_at' => '2018-08-21 13:06:21',
                'updated_at' => '2018-08-21 13:06:21',
                'deleted_at' => NULL,
                'payment_system_id' => 2,
                'code' => '9219312312',
                'name' => 'Галустян Галуст Галустянович',
                'reserve' => '0.000000',
                'active' => 1,
                'max_limit' => '0.000000',
                'fields' => '{"fio":"\\u0413\\u0430\\u043b\\u0443\\u0441\\u0442\\u044f\\u043d \\u0413\\u0430\\u043b\\u0443\\u0441\\u0442 \\u0413\\u0430\\u043b\\u0443\\u0441\\u0442\\u044f\\u043d\\u043e\\u0432\\u0438\\u0447","phone":"123123123"}',
            ),
            3 => 
            array (
                'id' => 5,
                'created_at' => '2018-08-21 13:06:52',
                'updated_at' => '2018-08-21 13:06:52',
                'deleted_at' => NULL,
                'payment_system_id' => 2,
                'code' => '7567567567',
                'name' => 'Маринова Марина Мариновна',
                'reserve' => '0.000000',
                'active' => 1,
                'max_limit' => '0.000000',
                'fields' => '{"fio":"\\u041c\\u0430\\u0440\\u0438\\u043d\\u0430","phone":"+1231231223213"}',
            ),
            4 => 
            array (
                'id' => 6,
                'created_at' => '2018-10-14 17:02:48',
                'updated_at' => '2018-10-14 17:02:48',
                'deleted_at' => NULL,
                'payment_system_id' => 3,
                'code' => '123123',
                'name' => 'Иванов Петр Петрович',
                'reserve' => '123123.000000',
                'active' => 1,
                'max_limit' => '0.000000',
                'fields' => '{"\\u041a\\u0430\\u043a\\u043e\\u0435 - \\u0442\\u043e \\u043f\\u043e\\u043b\\u0435 \\u0434\\u043b\\u044f \\u043f\\u0435\\u0440\\u0435\\u0432\\u043e\\u0434\\u0430":"123123123"}',
            ),
            5 => 
            array (
                'id' => 7,
                'created_at' => '2018-10-14 17:03:24',
                'updated_at' => '2018-10-14 17:03:34',
                'deleted_at' => NULL,
                'payment_system_id' => 3,
                'code' => '91919292',
                'name' => 'Мария',
                'reserve' => '889999.000000',
                'active' => 1,
                'max_limit' => '0.000000',
                'fields' => '{"\\u041a\\u0430\\u043a\\u043e\\u0435 - \\u0442\\u043e \\u043f\\u043e\\u043b\\u0435 \\u0434\\u043b\\u044f \\u043f\\u0435\\u0440\\u0435\\u0432\\u043e\\u0434\\u0430":"123123123"}',
            ),
        ));
        
        
    }
}