<?php

use Illuminate\Database\Seeder;

class AdminMenuTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_menu')->delete();
        
        \DB::table('admin_menu')->insert(array (
            0 => 
            array (
                'id' => 1,
                'parent_id' => 0,
                'order' => 1,
                'title' => 'Index',
                'icon' => 'fa-bar-chart',
                'uri' => '/',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'parent_id' => 0,
                'order' => 2,
                'title' => 'Admin',
                'icon' => 'fa-tasks',
                'uri' => '',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'parent_id' => 2,
                'order' => 3,
                'title' => 'Users',
                'icon' => 'fa-users',
                'uri' => 'auth/users',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'parent_id' => 2,
                'order' => 4,
                'title' => 'Roles',
                'icon' => 'fa-user',
                'uri' => 'auth/roles',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'parent_id' => 2,
                'order' => 5,
                'title' => 'Permission',
                'icon' => 'fa-ban',
                'uri' => 'auth/permissions',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'parent_id' => 2,
                'order' => 6,
                'title' => 'Menu',
                'icon' => 'fa-bars',
                'uri' => 'auth/menu',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'parent_id' => 2,
                'order' => 7,
                'title' => 'Operation log',
                'icon' => 'fa-history',
                'uri' => 'auth/logs',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'parent_id' => 0,
                'order' => 22,
                'title' => 'Страницы сайта',
                'icon' => 'fa-cube',
                'uri' => NULL,
                'created_at' => '2018-08-19 17:15:39',
                'updated_at' => '2018-08-20 07:22:32',
            ),
            8 => 
            array (
                'id' => 9,
                'parent_id' => 8,
                'order' => 25,
                'title' => 'FAQ',
                'icon' => 'fa-question',
                'uri' => 'pages/faq',
                'created_at' => '2018-08-19 17:16:13',
                'updated_at' => '2018-08-20 07:22:32',
            ),
            9 => 
            array (
                'id' => 10,
                'parent_id' => 8,
                'order' => 27,
                'title' => 'Новости',
                'icon' => 'fa-newspaper-o',
                'uri' => 'pages/news',
                'created_at' => '2018-08-19 17:16:49',
                'updated_at' => '2018-08-20 07:22:32',
            ),
            10 => 
            array (
                'id' => 11,
                'parent_id' => 8,
                'order' => 26,
                'title' => 'Контакты',
                'icon' => 'fa-map-marker',
                'uri' => 'pages/contacts',
                'created_at' => '2018-08-19 17:18:08',
                'updated_at' => '2018-08-20 07:22:32',
            ),
            11 => 
            array (
                'id' => 12,
                'parent_id' => 8,
                'order' => 24,
                'title' => 'Партнерам',
                'icon' => 'fa-users',
                'uri' => 'pages/partners',
                'created_at' => '2018-08-19 17:19:14',
                'updated_at' => '2018-08-20 07:22:32',
            ),
            12 => 
            array (
                'id' => 13,
                'parent_id' => 8,
                'order' => 23,
                'title' => 'Правила обмена',
                'icon' => 'fa-th-list',
                'uri' => 'pages/rules',
                'created_at' => '2018-08-19 17:19:42',
                'updated_at' => '2018-08-20 07:22:32',
            ),
            13 => 
            array (
                'id' => 14,
                'parent_id' => 0,
                'order' => 28,
                'title' => 'Настройки',
                'icon' => 'fa-gears',
                'uri' => NULL,
                'created_at' => '2018-08-19 17:20:46',
                'updated_at' => '2018-08-20 07:22:32',
            ),
            14 => 
            array (
                'id' => 15,
                'parent_id' => 14,
                'order' => 29,
                'title' => 'Список языков',
                'icon' => 'fa-globe',
                'uri' => 'settings/languages',
                'created_at' => '2018-08-19 17:21:12',
                'updated_at' => '2018-08-20 07:22:32',
            ),
            15 => 
            array (
                'id' => 17,
                'parent_id' => 14,
                'order' => 30,
                'title' => 'Категории FAQ',
                'icon' => 'fa-bars',
                'uri' => 'settings/faq-categories',
                'created_at' => '2018-08-19 17:23:01',
                'updated_at' => '2018-08-20 07:22:32',
            ),
            16 => 
            array (
                'id' => 18,
                'parent_id' => 14,
                'order' => 31,
                'title' => 'Статусы рефералов',
                'icon' => 'fa-user-secret',
                'uri' => 'settings/referal-statuses',
                'created_at' => '2018-08-19 17:23:41',
                'updated_at' => '2018-08-20 07:22:32',
            ),
            17 => 
            array (
                'id' => 19,
                'parent_id' => 14,
                'order' => 32,
                'title' => 'Статусы логов',
                'icon' => 'fa-list-alt',
                'uri' => 'settings/log-statuses',
                'created_at' => '2018-08-19 17:24:32',
                'updated_at' => '2018-08-20 07:22:32',
            ),
            18 => 
            array (
                'id' => 20,
                'parent_id' => 0,
                'order' => 8,
                'title' => 'Заявки',
                'icon' => 'fa-arrow-right',
                'uri' => 'bids',
                'created_at' => '2018-08-19 17:25:52',
                'updated_at' => '2018-08-19 17:31:14',
            ),
            19 => 
            array (
                'id' => 21,
                'parent_id' => 20,
                'order' => 9,
                'title' => 'Новые',
                'icon' => 'fa-list-ol',
                'uri' => 'bids/new',
                'created_at' => '2018-08-19 17:26:54',
                'updated_at' => '2018-08-19 17:31:14',
            ),
            20 => 
            array (
                'id' => 22,
                'parent_id' => 20,
                'order' => 10,
                'title' => 'Обработанные',
                'icon' => 'fa-check-circle-o',
                'uri' => 'bids/success',
                'created_at' => '2018-08-19 17:28:07',
                'updated_at' => '2018-08-19 17:31:14',
            ),
            21 => 
            array (
                'id' => 23,
                'parent_id' => 20,
                'order' => 11,
                'title' => 'Ошибочные',
                'icon' => 'fa-slack',
                'uri' => 'bids/error',
                'created_at' => '2018-08-19 17:29:25',
                'updated_at' => '2018-08-19 17:31:14',
            ),
            22 => 
            array (
                'id' => 24,
                'parent_id' => 20,
                'order' => 12,
                'title' => 'Удаленные',
                'icon' => 'fa-trash',
                'uri' => 'bids/trash',
                'created_at' => '2018-08-19 17:29:45',
                'updated_at' => '2018-08-19 17:31:14',
            ),
            23 => 
            array (
                'id' => 25,
                'parent_id' => 20,
                'order' => 13,
                'title' => 'Замороженные',
                'icon' => 'fa-asterisk',
                'uri' => 'bids/freeze',
                'created_at' => '2018-08-19 17:30:11',
                'updated_at' => '2018-08-19 17:31:14',
            ),
            24 => 
            array (
                'id' => 26,
                'parent_id' => 20,
                'order' => 14,
                'title' => 'Предварительные',
                'icon' => 'fa-clock-o',
                'uri' => 'bids/wait',
                'created_at' => '2018-08-19 17:31:02',
                'updated_at' => '2018-08-19 17:31:14',
            ),
            25 => 
            array (
                'id' => 27,
                'parent_id' => 0,
                'order' => 15,
                'title' => 'Управление',
                'icon' => 'fa-circle-o-notch',
                'uri' => NULL,
                'created_at' => '2018-08-19 17:32:34',
                'updated_at' => '2018-08-19 17:34:39',
            ),
            26 => 
            array (
                'id' => 28,
                'parent_id' => 27,
                'order' => 21,
                'title' => 'Кошельки',
                'icon' => 'fa-briefcase',
                'uri' => 'control/wallets',
                'created_at' => '2018-08-19 17:33:12',
                'updated_at' => '2018-08-20 09:16:28',
            ),
            27 => 
            array (
                'id' => 29,
                'parent_id' => 27,
                'order' => 16,
                'title' => 'Курс обмена',
                'icon' => 'fa-percent',
                'uri' => 'control/cource',
                'created_at' => '2018-08-19 17:33:36',
                'updated_at' => '2018-08-20 09:16:28',
            ),
            28 => 
            array (
                'id' => 30,
                'parent_id' => 27,
                'order' => 17,
                'title' => 'Резервы валют',
                'icon' => 'fa-align-left',
                'uri' => 'control/reserve',
                'created_at' => '2018-08-19 17:34:06',
                'updated_at' => '2018-08-20 09:16:28',
            ),
            29 => 
            array (
                'id' => 31,
                'parent_id' => 27,
                'order' => 18,
                'title' => 'Пользователи',
                'icon' => 'fa-user',
                'uri' => 'control/guests',
                'created_at' => '2018-08-19 17:34:28',
                'updated_at' => '2018-08-26 13:00:53',
            ),
            30 => 
            array (
                'id' => 32,
                'parent_id' => 27,
                'order' => 19,
                'title' => 'Сервисы',
                'icon' => 'fa-bold',
                'uri' => 'control/payments',
                'created_at' => '2018-08-20 07:20:42',
                'updated_at' => '2018-08-20 09:16:28',
            ),
            31 => 
            array (
                'id' => 33,
                'parent_id' => 27,
                'order' => 20,
                'title' => 'Дополнительные поля',
                'icon' => 'fa-bars',
                'uri' => 'control/payments-fields',
                'created_at' => '2018-08-20 07:22:07',
                'updated_at' => '2018-08-20 09:16:28',
            ),
        ));
        
        
    }
}