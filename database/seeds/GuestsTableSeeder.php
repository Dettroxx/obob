<?php

use Illuminate\Database\Seeder;

class GuestsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('guests')->delete();
        
        \DB::table('guests')->insert(array (
            0 => 
            array (
                'id' => 17,
                'ip' => '127.0.0.1',
                'name' => 'Ivan',
                'email' => 'Ivan@ya.ru',
                'status' => 0,
                'earning_percent' => '0.00',
                'remuneration_percent' => '0.00',
                'is_shop' => 0,
                'shop_percent' => '0.000000',
                'alerts' => 0,
                'income' => 0,
                'created_at' => '2018-10-14 17:36:36',
                'updated_at' => '2018-10-14 17:38:05',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}