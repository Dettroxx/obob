<?php

use Illuminate\Database\Seeder;

class PaymentLinksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payment_links')->delete();
        
        \DB::table('payment_links')->insert(array (
            0 => 
            array (
                'id' => 2,
                'from' => 2,
                'to' => 3,
                'active' => 0,
                'reverse' => 0,
                'course' => '69.00',
                'extra' => '5.50',
                'corridor_from' => '0.000000',
                'corridor_to' => '0.000000',
                'corridor_active' => 0,
                'alert' => 0,
            ),
            1 => 
            array (
                'id' => 3,
                'from' => 1,
                'to' => 2,
                'active' => 1,
                'reverse' => 1,
                'course' => '120.00',
                'extra' => '13.00',
                'corridor_from' => '0.000000',
                'corridor_to' => '0.000000',
                'corridor_active' => 1,
                'alert' => 1,
            ),
        ));
        
        
    }
}