<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('languages')->delete();
        
        \DB::table('languages')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => NULL,
                'updated_at' => '2018-08-19 18:40:11',
                'key' => 'ru',
                'name' => 'Russian',
                'icon' => 'images/255px-Flag_of_Russia.svg.png',
            ),
        ));
        
        
    }
}