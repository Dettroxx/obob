<?php

use Illuminate\Database\Seeder;

class AdminUsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_users')->delete();
        
        \DB::table('admin_users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'username' => 'admin',
                'password' => '$2y$10$tjL7WeBr0pDnum8qMwkHhOSOAjHXxgXDh2WsbjJyLHgS1PLumgRVK',
                'name' => 'Administrator',
                'avatar' => NULL,
                'remember_token' => 'JCsZTgnDI3bVonV8eY2Uc3rQredbPS4Q9dNcFzt8sgqt0rN04ISVZtoGuCzH',
                'created_at' => '2018-08-19 17:01:41',
                'updated_at' => '2018-08-19 17:01:41',
            ),
            1 => 
            array (
                'id' => 2,
                'username' => 'ivan777',
                'password' => '$2y$10$I1Bq4GKmlvQuFgZhDcMR1.24tnvJOTaY/7yHj6pp2Rl./yP1M.SSO',
                'name' => 'Иван',
                'avatar' => NULL,
                'remember_token' => NULL,
                'created_at' => '2018-08-26 13:03:13',
                'updated_at' => '2018-08-26 13:03:13',
            ),
            2 => 
            array (
                'id' => 9,
                'username' => 'user1@ya.ru',
                'password' => '$2y$10$Isp8x6x7TNTvnlaXVdn30Oaj0eiCallM33xj/2VMFnN8Hv5sZwXAO',
                'name' => 'user1@ya.ru',
                'avatar' => NULL,
                'remember_token' => NULL,
                'created_at' => '2018-10-01 01:34:01',
                'updated_at' => '2018-10-01 01:47:41',
            ),
            3 => 
            array (
                'id' => 10,
                'username' => 'user1@mail.ru',
                'password' => '$2y$10$T57CqgnDpJrEUrS0xBz.auzdw0Uj/yfrwsv72sKOKGWtwy.3ozIhG',
                'name' => 'Петр',
                'avatar' => NULL,
                'remember_token' => NULL,
                'created_at' => '2018-10-01 01:46:31',
                'updated_at' => '2018-10-01 01:57:19',
            ),
            4 => 
            array (
                'id' => 11,
                'username' => 'Otherfields',
                'password' => '$2y$10$FUU8DUsA2rQwJg5Skgo6E.HqwJZaAO2mt8bQnGnUkdHsYOqRhLZSq',
                'name' => 'Somefields',
                'avatar' => NULL,
                'remember_token' => NULL,
                'created_at' => '2018-10-14 15:00:57',
                'updated_at' => '2018-10-14 15:00:57',
            ),
            5 => 
            array (
                'id' => 12,
                'username' => 'Somemail@ya.ru',
                'password' => '$2y$10$.KNomWYK.jIrMHpEuoZWleynxyFJtwNmLoH.5.C8p8gW.Nx5Ks7mi',
                'name' => 'Сергей',
                'avatar' => NULL,
                'remember_token' => NULL,
                'created_at' => '2018-10-14 15:03:34',
                'updated_at' => '2018-10-14 16:52:22',
            ),
            6 => 
            array (
                'id' => 13,
                'username' => 'asdasd2@asda.ru',
                'password' => '$2y$10$gVWy9mgRxxQtrm5bHg1cru2uGMJTWDCIn6vrrGoR1XyNFRTItt1hu',
                'name' => 'asdasd',
                'avatar' => NULL,
                'remember_token' => NULL,
                'created_at' => '2018-10-14 15:16:01',
                'updated_at' => '2018-10-14 15:17:48',
            ),
            7 => 
            array (
                'id' => 14,
                'username' => 'asd',
                'password' => '$2y$10$/EZ15Aoaw9FEQ7Q6iMlV6uglEQbW8UADFozvtrg.7e5fWFk2pnsdO',
                'name' => 'asdasd',
                'avatar' => NULL,
                'remember_token' => NULL,
                'created_at' => '2018-10-14 15:19:20',
                'updated_at' => '2018-10-14 15:19:20',
            ),
            8 => 
            array (
                'id' => 15,
                'username' => '123123',
                'password' => '$2y$10$wUmvk3RiPaRV3YJrt8KSc.lyBnKGlQz9qZuK4FiRw/AYMRamFWSBa',
                'name' => '123123',
                'avatar' => NULL,
                'remember_token' => NULL,
                'created_at' => '2018-10-14 15:20:53',
                'updated_at' => '2018-10-14 15:51:14',
            ),
            9 => 
            array (
                'id' => 16,
                'username' => 'asasdasd',
                'password' => '$2y$10$EqBfXOPV.dzXjCp9IiZnhu9VfDUw8Ni3sjcof/goxdrHWa/nEmGLG',
                'name' => 'asdasd',
                'avatar' => NULL,
                'remember_token' => NULL,
                'created_at' => '2018-10-14 17:09:48',
                'updated_at' => '2018-10-14 17:09:48',
            ),
            10 => 
            array (
                'id' => 17,
                'username' => 'Ivan@ya.ru',
                'password' => '$2y$10$wgURhAIhthYTD4pyGC6Jg.Xdrui6fG.FqG22dgW2Oafksw1oDwxfq',
                'name' => 'Ivan',
                'avatar' => NULL,
                'remember_token' => NULL,
                'created_at' => '2018-10-14 17:36:36',
                'updated_at' => '2018-10-14 17:38:05',
            ),
        ));
        
        
    }
}