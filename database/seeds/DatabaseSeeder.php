<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(AdminMenuTableSeeder::class);
        $this->call(AdminPermissionsTableSeeder::class);
        $this->call(AdminRolesTableSeeder::class);
        $this->call(AdminRoleMenuTableSeeder::class);
        $this->call(AdminRolePermissionsTableSeeder::class);
        $this->call(AdminRoleUsersTableSeeder::class);
        $this->call(AdminUsersTableSeeder::class);
        $this->call(AdminUserPermissionsTableSeeder::class);
        $this->call(LanguagesTableSeeder::class);
        $this->call(FaqCategoriesTableSeeder::class);
        $this->call(FaqsTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(PaymentSystemsTableSeeder::class);
        $this->call(PaymentFieldsTableSeeder::class);
        $this->call(WalletsTableSeeder::class);
        $this->call(GuestsTableSeeder::class);
        $this->call(GuestsReferalLinksTableSeeder::class);
        $this->call(PaymentLinksTableSeeder::class);
        $this->call(BidStatusesTableSeeder::class);
        $this->call(BidsTableSeeder::class);
        $this->call(PaymentFieldValuesTableSeeder::class);
        $this->call(BidFieldValuesTableSeeder::class);
        $this->call(BidFieldsLinksTableSeeder::class);
        $this->call(GuestStatusesTableSeeder::class);
        $this->call(RefferalAccrualsTableSeeder::class);
        $this->call(WalletFieldsLinkTableSeeder::class);
        $this->call(WalletLogTableSeeder::class);
    }
}
