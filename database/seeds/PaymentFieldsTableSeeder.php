<?php

use Illuminate\Database\Seeder;

class PaymentFieldsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('payment_fields')->delete();
        
        \DB::table('payment_fields')->insert(array (
            0 => 
            array (
                'id' => 1,
                'payment_system_id' => 2,
                'key' => 'fio',
                'info' => 'ФИО',
                'icon' => 'fa-anchor',
                'required' => 1,
                'mask' => 'R 0000 0000 00000',
                'show_out' => 1,
                'show_in' => 1,
                'show_wallet' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'payment_system_id' => 2,
                'key' => 'phone',
                'info' => 'Телефон',
                'icon' => 'fa-pencil',
                'required' => 0,
                'mask' => '0000-0000-0000',
                'show_out' => 1,
                'show_in' => 1,
                'show_wallet' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'payment_system_id' => 3,
                'key' => 'Какое - то поле для перевода',
                'info' => 'somefield',
                'icon' => 'fa-pencil',
                'required' => 1,
                'mask' => 'KZ 000 00000 0000',
                'show_out' => 1,
                'show_in' => 1,
                'show_wallet' => 1,
            ),
        ));
        
        
    }
}