<?php

use Illuminate\Database\Seeder;

class BidStatusesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('bid_statuses')->delete();
        
        \DB::table('bid_statuses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Новая',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Обработанная',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Ошибочная',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Замороженная',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Предварительная',
            ),
        ));
        
        
    }
}