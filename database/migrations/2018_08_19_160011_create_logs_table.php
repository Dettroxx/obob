<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogsTable extends Migration {

	public function up()
	{
		Schema::create('logs', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->enum('owner_type', array('operator', 'guest', 'request', 'wallet', 'system'))->index();
			$table->integer('owner_id')->unsigned()->index();
			$table->integer('status')->unsigned()->index();
			$table->text('data')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('logs');
	}
}