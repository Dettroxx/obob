<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentFieldsTable extends Migration {

	public function up()
	{
		Schema::create('payment_fields', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('payment_system_id')->unsigned()->index();
			$table->string('key', 50)->index();
			$table->string('info');
			$table->text('icon');
			$table->tinyInteger('required')->default('0');
			$table->string('mask')->nullable();
			$table->tinyInteger('show_out')->index()->default('0');
			$table->tinyInteger('show_in')->index()->default('0');
			$table->tinyInteger('show_wallet')->index()->default('0');
		});
	}

	public function down()
	{
		Schema::drop('payment_fields');
	}
}