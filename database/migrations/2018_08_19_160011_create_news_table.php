<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNewsTable extends Migration {

	public function up()
	{
		Schema::create('news', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('lang')->unsigned()->index();
			$table->string('name');
			$table->string('alias')->nullable();
			$table->text('introtext')->nullable();
			$table->text('content')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('news');
	}
}