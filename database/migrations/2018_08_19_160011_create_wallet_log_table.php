<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWalletLogTable extends Migration {

	public function up()
	{
		Schema::create('wallet_log', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('wallet')->unsigned();
			$table->tinyInteger('in_out')->index()->default('0');
			$table->integer('bid_id')->unsigned()->index();
			$table->decimal('cost', 14,6)->default('0');
			$table->integer('online')->index()->default('0');
		});
	}

	public function down()
	{
		Schema::drop('wallet_log');
	}
}