<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentFieldValuesTable extends Migration {

	public function up()
	{
		Schema::create('payment_field_values', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('payment_field')->unsigned();
			$table->text('value')->nullable();
			$table->tinyInteger('out_in')->index()->default('0');
		});
	}

	public function down()
	{
		Schema::drop('payment_field_values');
	}
}