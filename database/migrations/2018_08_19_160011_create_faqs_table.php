<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFaqsTable extends Migration {

	public function up()
	{
		Schema::create('faqs', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('category_id')->unsigned();
			$table->text('name')->nullable();
			$table->string('alias')->nullable();
			$table->integer('order')->unsigned()->index()->default('0');
			$table->text('content')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('faqs');
	}
}