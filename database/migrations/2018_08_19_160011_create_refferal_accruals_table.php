<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRefferalAccrualsTable extends Migration {

	public function up()
	{
		Schema::create('refferal_accruals', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('guest_id')->unsigned()->index()->default('0');
			$table->decimal('total', 14,6)->default('0');
			$table->decimal('account', 14,6)->default('0');
			$table->integer('payment_system_id')->unsigned()->index();
			$table->enum('type', array('referal', 'fee', 'in', 'out', 'shop_in', 'shop_out', 'shop_account'))->index();
		});
	}

	public function down()
	{
		Schema::drop('refferal_accruals');
	}
}