<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGuestsReferalLinksTable extends Migration {

	public function up()
	{
		Schema::create('guests_referal_links', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('from')->unsigned()->index();
			$table->integer('to')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('guests_referal_links');
	}
}