<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGuestsTable extends Migration {

	public function up()
	{
		Schema::create('guests', function(Blueprint $table) {
			$table->integer("id")->unsigned()->unique();
			$table->string('ip', 20);
			$table->string('name')->nullable();
			$table->string('email');
			$table->integer('status')->unsigned()->index()->default(0);
			$table->decimal('earning_percent', 4,2)->default('0');
			$table->decimal('remuneration_percent', 4,2)->default('0');
			$table->tinyInteger('is_shop')->index()->default('0');
			$table->decimal('shop_percent', 14,6)->default(0);
			$table->tinyInteger('alerts')->unsigned()->default('0');
			$table->tinyInteger('income')->index()->default('0');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('guests');
	}
}