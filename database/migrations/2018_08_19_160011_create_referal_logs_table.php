<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReferalLogsTable extends Migration {

	public function up()
	{
		Schema::create('referal_logs', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('bid_id')->unsigned()->index();
			$table->integer('payment_id')->unsigned()->index();
			$table->integer('guest_id')->unsigned()->index();
		});
	}

	public function down()
	{
		Schema::drop('referal_logs');
	}
}