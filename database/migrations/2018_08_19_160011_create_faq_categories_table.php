<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFaqCategoriesTable extends Migration {

	public function up()
	{
		Schema::create('faq_categories', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('lang')->unsigned()->index();
			$table->string('name');
			$table->string('alias')->nullable();
			$table->integer('order')->unsigned()->index()->default('0');
		});
	}

	public function down()
	{
		Schema::drop('faq_categories');
	}
}