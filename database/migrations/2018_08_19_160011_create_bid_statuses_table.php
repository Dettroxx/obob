<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBidStatusesTable extends Migration {

	public function up()
	{
		Schema::create('bid_statuses', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
		});
	}

	public function down()
	{
		Schema::drop('bid_statuses');
	}
}