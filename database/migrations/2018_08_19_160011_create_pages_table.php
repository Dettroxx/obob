<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagesTable extends Migration {

	public function up()
	{
		Schema::create('pages', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('lang')->unsigned()->index();
			$table->string('family')->index(); //Семейство 
			$table->string('name');
			$table->string('alias')->nullable()->index();
			$table->text('content')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('pages');
	}
}