<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentLinksTable extends Migration {

	public function up()
	{
		Schema::create('payment_links', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('from')->unsigned()->index();
			$table->integer('to')->unsigned()->index();
			$table->tinyInteger('active')->index()->default('0');
			$table->tinyInteger('reverse')->index()->default('0');
			$table->decimal('course', 8,2)->default('0');
			$table->decimal('extra', 8,2)->default('0'); 
			$table->decimal('corridor_from', 14,6)->default('0');
			$table->decimal('corridor_to', 14,6)->default('0');
			$table->tinyInteger('corridor_active')->default('0');
			$table->tinyInteger('alert')->default('0');
		});
	}

	public function down()
	{
		Schema::drop('payment_links');
	}
}