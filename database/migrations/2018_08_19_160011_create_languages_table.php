<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLanguagesTable extends Migration {

	public function up()
	{
		Schema::create('languages', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('key', 3)->unique();
			$table->string('name', 20)->nullable();
			$table->text('icon')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('languages');
	}
}