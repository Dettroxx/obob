<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWalletFieldsLinkTable extends Migration {

	public function up()
	{
		Schema::create('wallet_fields_link', function(Blueprint $table) {
			$table->integer('wallet_id')->unsigned()->index();
			$table->integer('value_id')->unsigned()->index();
		});
	}

	public function down()
	{
		Schema::drop('wallet_fields_link');
	}
}