<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateBidsFieldValuesTable extends Migration {

	public function up() 
	{
		Schema::create('bid_field_values', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('bid')->unsigned();
			$table->integer('field_id')->unsigned();
			$table->text('value')->nullable();
			$table->tinyInteger('out_in')->index()->default('0');

			//Связи
			$table->foreign('bid')->references('id')->on('bids')
			->onDelete('cascade')
			->onUpdate('cascade');

			$table->foreign('field_id')->references('id')->on('payment_fields')
			->onDelete('cascade')
			->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::drop('bid_field_values');
	}
}