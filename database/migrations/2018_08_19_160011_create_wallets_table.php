<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWalletsTable extends Migration {

	public function up()
	{
		Schema::create('wallets', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('payment_system_id')->unsigned()->index();
			$table->string('code')->index();
			$table->string('name');
			$table->decimal('reserve', 14,6);
			$table->tinyInteger('active')->index()->default('0');
			$table->decimal('max_limit', 14,6)->default('0');
			$table->text('fields')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('wallets');
	}
}