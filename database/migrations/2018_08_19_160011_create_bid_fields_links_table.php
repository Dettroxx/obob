<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBidFieldsLinksTable extends Migration {

	public function up()
	{
		Schema::create('bid_fields_links', function(Blueprint $table) {
			$table->integer('bid_id')->unsigned();
			$table->integer('value_id')->unsigned()->index();
		});
	}

	public function down()
	{
		Schema::drop('bid_fields_links');
	}
}