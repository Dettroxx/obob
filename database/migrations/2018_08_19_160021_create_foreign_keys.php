<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('faqs', function(Blueprint $table) {
			$table->foreign('category_id')->references('id')->on('faq_categories')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('faq_categories', function(Blueprint $table) {
			$table->foreign('lang')->references('id')->on('languages')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('news', function(Blueprint $table) {
			$table->foreign('lang')->references('id')->on('languages')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('pages', function(Blueprint $table) {
			$table->foreign('lang')->references('id')->on('languages')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('bids', function(Blueprint $table) {
			$table->foreign('from')->references('id')->on('payment_systems')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('bids', function(Blueprint $table) {
			$table->foreign('to')->references('id')->on('payment_systems')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('bids', function(Blueprint $table) {
			$table->foreign('status')->references('id')->on('bid_statuses')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('wallets', function(Blueprint $table) {
			$table->foreign('payment_system_id')->references('id')->on('payment_systems')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('wallet_log', function(Blueprint $table) {
			$table->foreign('wallet')->references('id')->on('wallets')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('wallet_log', function(Blueprint $table) {
			$table->foreign('bid_id')->references('id')->on('bids')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('payment_fields', function(Blueprint $table) {
			$table->foreign('payment_system_id')->references('id')->on('payment_systems')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('payment_field_values', function(Blueprint $table) {
			$table->foreign('payment_field')->references('id')->on('payment_fields')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('bid_fields_links', function(Blueprint $table) {
			$table->foreign('bid_id')->references('id')->on('bids')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('bid_fields_links', function(Blueprint $table) {
			$table->foreign('value_id')->references('id')->on('payment_field_values')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('wallet_fields_link', function(Blueprint $table) {
			$table->foreign('wallet_id')->references('id')->on('wallets')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('wallet_fields_link', function(Blueprint $table) {
			$table->foreign('value_id')->references('id')->on('payment_field_values')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('payment_links', function(Blueprint $table) {
			$table->foreign('from')->references('id')->on('payment_systems')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('payment_links', function(Blueprint $table) {
			$table->foreign('to')->references('id')->on('payment_systems')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('guests', function(Blueprint $table) {
			$table->foreign('id')->references('id')->on('admin_users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('guests_referal_links', function(Blueprint $table) {
			$table->foreign('from')->references('id')->on('guests')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('guests_referal_links', function(Blueprint $table) {
			$table->foreign('to')->references('id')->on('guests')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('refferal_accruals', function(Blueprint $table) {
			$table->foreign('guest_id')->references('id')->on('guests')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('refferal_accruals', function(Blueprint $table) {
			$table->foreign('payment_system_id')->references('id')->on('payment_systems')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('referal_logs', function(Blueprint $table) {
			$table->foreign('bid_id')->references('id')->on('bids')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('referal_logs', function(Blueprint $table) {
			$table->foreign('payment_id')->references('id')->on('payment_systems')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('referal_logs', function(Blueprint $table) {
			$table->foreign('guest_id')->references('id')->on('guests')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('logs', function(Blueprint $table) {
			$table->foreign('status')->references('id')->on('log_statuses')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	} 

	public function down()
	{
		Schema::table('faqs', function(Blueprint $table) {
			$table->dropForeign('faqs_category_id_foreign');
		});
		Schema::table('faq_categories', function(Blueprint $table) {
			$table->dropForeign('faq_categories_lang_foreign');
		});
		Schema::table('news', function(Blueprint $table) {
			$table->dropForeign('news_lang_foreign');
		});
		Schema::table('pages', function(Blueprint $table) {
			$table->dropForeign('pages_lang_foreign');
		});
		Schema::table('bids', function(Blueprint $table) {
			$table->dropForeign('bids_from_foreign');
		});
		Schema::table('bids', function(Blueprint $table) {
			$table->dropForeign('bids_to_foreign');
		});
		Schema::table('bids', function(Blueprint $table) {
			$table->dropForeign('bids_status_foreign');
		});
		Schema::table('wallets', function(Blueprint $table) {
			$table->dropForeign('wallets_payment_system_id_foreign');
		});
		Schema::table('wallet_log', function(Blueprint $table) {
			$table->dropForeign('wallet_log_wallet_foreign');
		});
		Schema::table('wallet_log', function(Blueprint $table) {
			$table->dropForeign('wallet_log_bid_id_foreign');
		});
		Schema::table('payment_fields', function(Blueprint $table) {
			$table->dropForeign('payment_fields_payment_system_id_foreign');
		});
		Schema::table('payment_field_values', function(Blueprint $table) {
			$table->dropForeign('payment_field_values_payment_field_foreign');
		});
		Schema::table('bid_fields_links', function(Blueprint $table) {
			$table->dropForeign('bid_fields_links_bid_id_foreign');
		});
		Schema::table('bid_fields_links', function(Blueprint $table) {
			$table->dropForeign('bid_fields_links_value_id_foreign');
		});
		Schema::table('wallet_fields_link', function(Blueprint $table) {
			$table->dropForeign('wallet_fields_link_wallet_id_foreign');
		});
		Schema::table('wallet_fields_link', function(Blueprint $table) {
			$table->dropForeign('wallet_fields_link_value_id_foreign');
		});
		Schema::table('payment_links', function(Blueprint $table) {
			$table->dropForeign('payment_links_from_foreign');
		});
		Schema::table('payment_links', function(Blueprint $table) {
			$table->dropForeign('payment_links_to_foreign');
		});
		Schema::table('guests_referal_links', function(Blueprint $table) {
			$table->dropForeign('guests_referal_links_from_foreign');
		});
		Schema::table('guests_referal_links', function(Blueprint $table) {
			$table->dropForeign('guests_referal_links_to_foreign');
		});
		Schema::table('refferal_accruals', function(Blueprint $table) {
			$table->dropForeign('refferal_accruals_guest_id_foreign');
		});
		Schema::table('refferal_accruals', function(Blueprint $table) {
			$table->dropForeign('refferal_accruals_payment_system_id_foreign');
		});
		Schema::table('referal_logs', function(Blueprint $table) {
			$table->dropForeign('referal_logs_bid_id_foreign');
		});
		Schema::table('referal_logs', function(Blueprint $table) {
			$table->dropForeign('referal_logs_payment_id_foreign');
		});
		Schema::table('referal_logs', function(Blueprint $table) {
			$table->dropForeign('referal_logs_guest_id_foreign');
		});
		Schema::table('logs', function(Blueprint $table) {
			$table->dropForeign('logs_status_foreign');
		});
	}
}