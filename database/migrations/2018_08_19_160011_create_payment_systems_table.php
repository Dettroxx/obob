<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentSystemsTable extends Migration {

	public function up()
	{
		Schema::create('payment_systems', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->text('icon')->nullable();
			$table->string('name');
			$table->tinyInteger('in')->index()->default('0');
			$table->tinyInteger('out')->index()->default('0');
			$table->tinyInteger('referal_out')->index()->default('0');
			$table->tinyInteger('precise')->default('2');
			$table->string('abbr', 10)->nullable();
			$table->tinyInteger('active')->index()->default('0');
			$table->enum('type', array('bank', 'pc', 'coin'));
		});
	}

	public function down()
	{
		Schema::drop('payment_systems');
	}
}