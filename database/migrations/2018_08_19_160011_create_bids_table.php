<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBidsTable extends Migration {

	public function up()
	{
		Schema::create('bids', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('from')->unsigned()->index();
			$table->integer('to')->unsigned()->index();
			$table->integer('operator')->unsigned()->index()->nullable();
			$table->integer('guest_id')->unsigned()->index();
			$table->integer('status')->unsigned()->index();
			$table->decimal('value_from', 14,6)->default('0');
			$table->decimal('value_to', 14,6)->default('0');
			$table->boolean('trash')->default(false);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('bids');
	}
}